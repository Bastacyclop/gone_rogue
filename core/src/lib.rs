extern crate num;
extern crate cgmath;

pub mod unit;
pub mod math;

pub trait Float: num::Float + cgmath::BaseNum {}

impl<T: num::Float + cgmath::BaseNum> Float for T {}
