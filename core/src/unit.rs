use std::marker::PhantomData;
use std::fmt;
use std::ops::*;
use std::cmp::*;
use num::{Num, Zero, One, NumCast, ToPrimitive, Float};
use cgmath;

pub struct Meter;

impl Unit for Meter {
    fn debug_fmt<T: fmt::Debug>(t: &T, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Meter({:?})", t)
    }
    
    fn display_fmt<T: fmt::Display>(t: &T, f: &mut fmt::Formatter) -> fmt::Result {
        let precision = f.precision().unwrap_or(3);
        write!(f, "{:.*} m", precision, t)
    }
}

pub type Meters<T: Copy = f64> = Value<Meter, T>;

pub fn meters<T: Copy>(t: T) -> Meters<T> {
    Value {
        value: t,
        phantom: PhantomData
    }
}

pub trait Unit {
    fn debug_fmt<T: fmt::Debug>(t: &T, f: &mut fmt::Formatter) -> fmt::Result;
    fn display_fmt<T: fmt::Display>(t: &T, f: &mut fmt::Formatter) -> fmt::Result;
}

pub struct Value<U: Unit, T: Copy> {
    value: T,
    phantom: PhantomData<U>,
}

impl<U: Unit, T: Copy> Value<U, T> {
    pub fn new(value: T) -> Self {
        Value {
            value: value,
            phantom: PhantomData
        }
    }
    
    pub fn get(self) -> T {
        self.value
    }
}

impl<U: Unit, T: Copy> Copy for Value<U, T> {}
impl<U: Unit, T: Copy> Clone for Value<U, T> {
    fn clone(&self) -> Self {
        Value::new(self.get())
    }
}

impl<U: Unit, T: Copy + fmt::Debug> fmt::Debug for Value<U, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        U::debug_fmt(&self.get(), f)
    }
}

impl<U: Unit, T: Copy + fmt::Display> fmt::Display for Value<U, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        U::display_fmt(&self.get(), f)
    }
}

impl<U: Unit, T: Num + Copy> Num for Value<U, T> {
    type FromStrRadixErr = T::FromStrRadixErr;
    
    fn from_str_radix(str: &str, radix: u32) -> Result<Self, Self::FromStrRadixErr> {
        T::from_str_radix(str, radix).map(|v| Value::new(v))
    }
}

impl<U: Unit, T: PartialEq + Copy> PartialEq for Value<U, T> {
    fn eq(&self, other: &Self) -> bool {
        self.get().eq(&other.get())
    }
}

impl<U: Unit, T: Zero + Copy> Zero for Value<U, T> {
    fn zero() -> Self {
        Value::new(T::zero())
    }
    
    fn is_zero(&self) -> bool {
        self.get().is_zero()
    }
}

impl<U: Unit, T: One + Copy> One for Value<U, T> {
    fn one() -> Self {
        Value::new(T::one())
    }
}

impl<U: Unit, T: Neg<Output=T> + Copy> Neg for Value<U, T> {
    type Output = Self;
    
    fn neg(self) -> Self::Output {
        Value::new(self.get().neg())
    }
}
 
impl<U: Unit, T: Add<Output=T> + Copy> Add for Value<U, T> {
    type Output = Self;
    
    fn add(self, rhs: Self) -> Self::Output {
        Value::new(self.get().add(rhs.get()))
    }
}
 
impl<U: Unit, T: Sub<Output=T> + Copy> Sub for Value<U, T> {
    type Output = Self;
    
    fn sub(self, rhs: Self) -> Self::Output {
        Value::new(self.get().sub(rhs.get()))
    }
}
 
impl<U: Unit, T: Mul<Output=T> + Copy> Mul for Value<U, T> {
    type Output = Self;
    
    fn mul(self, rhs: Self) -> Self::Output {
        Value::new(self.get().mul(rhs.get()))
    }
}

impl<U: Unit, T: Div<Output=T> + Copy> Div for Value<U, T> {
    type Output = Self;
    
    fn div(self, rhs: Self) -> Self::Output {
        Value::new(self.get().div(rhs.get()))
    }
}
 
impl<U: Unit, T: Rem<Output=T> + Copy> Rem for Value<U, T> {
    type Output = Self;
    
    fn rem(self, rhs: Self) -> Self::Output {
        Value::new(self.get().rem(rhs.get()))
    }
}

impl<U: Unit, T: AddAssign + Copy> AddAssign for Value<U, T> {
    fn add_assign(&mut self, rhs: Self) {
        self.value.add_assign(rhs.get());
    }
}

impl<U: Unit, T: SubAssign + Copy> SubAssign for Value<U, T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.value.sub_assign(rhs.get());
    }
}

impl<U: Unit, T: MulAssign + Copy> MulAssign for Value<U, T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.value.mul_assign(rhs.get());
    }
}

impl<U: Unit, T: DivAssign + Copy> DivAssign for Value<U, T> {
    fn div_assign(&mut self, rhs: Self) {
        self.value.div_assign(rhs.get());
    }
}

impl<U: Unit, T: RemAssign + Copy> RemAssign for Value<U, T> {
    fn rem_assign(&mut self, rhs: Self) {
        self.value.rem_assign(rhs.get());
    }
}

impl<U: Unit, T: NumCast + Copy> NumCast for Value<U, T> {
    fn from<P: ToPrimitive>(n: P) -> Option<Self> {
        T::from(n).map(|v| Value::new(v))
    }
}

impl<U: Unit, T: ToPrimitive + Copy> ToPrimitive for Value<U, T> {
    fn to_i64(&self) -> Option<i64> {
        self.get().to_i64()
    }
    
    fn to_u64(&self) -> Option<u64> {
        self.get().to_u64()
    }
}

impl<U: Unit, T: cgmath::BaseNum + Copy> cgmath::BaseNum for Value<U, T> {}
impl<U: Unit, T: cgmath::BaseInt + Copy> cgmath::BaseInt for Value<U, T> {}
impl<U: Unit, T: cgmath::BaseFloat + Copy> cgmath::BaseFloat for Value<U, T> {}

impl<U: Unit, T: cgmath::ApproxEq<Epsilon=T> + NumCast + Float + Copy> cgmath::ApproxEq for Value<U, T> {
    type Epsilon = Self;
    
    fn approx_eq_eps(&self, other: &Self, epsilon: &Self::Epsilon) -> bool {
        self.get().approx_eq_eps(&other.get(), &epsilon.get())
    }
}

impl<U: Unit, T: cgmath::PartialOrd + Copy> cgmath::PartialOrd for Value<U, T> {
    fn partial_min(self, other: Self) -> Self {
        Value::new(self.get().partial_min(other.get()))
    }
    
    fn partial_max(self, other: Self) -> Self {
        Value::new(self.get().partial_max(other.get()))
    }
}

impl<U: Unit, T: PartialOrd + Copy> PartialOrd for Value<U, T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.get().partial_cmp(&other.get())
    }
}

impl<U: Unit, T: Float + Copy> Float for Value<U, T> {
    fn nan() -> Self {
        Value::new(T::nan())
    }
    
    fn infinity() -> Self {
        Value::new(T::infinity())
    }
    
    fn neg_infinity() -> Self {
        Value::new(T::neg_infinity())
    }
    
    fn neg_zero() -> Self {
        Value::new(T::neg_zero())
    }
    
    fn min_value() -> Self {
        Value::new(T::min_value())
    }
    
    fn min_positive_value() -> Self {
        Value::new(T::min_positive_value())
    }
    
    fn max_value() -> Self {
        Value::new(T::max_value())
    }
    
    fn is_nan(self) -> bool {
        self.get().is_nan()
    }
    
    fn is_infinite(self) -> bool {
        self.get().is_infinite()
    }
    
    fn is_finite(self) -> bool {
        self.get().is_finite()
    }
    
    fn is_normal(self) -> bool {
        self.get().is_normal()
    }
    
    fn classify(self) -> ::std::num::FpCategory {
        self.get().classify()
    }
    
    fn floor(self) -> Self {
        Value::new(self.get().floor())
    }
    
    fn ceil(self) -> Self {
        Value::new(self.get().ceil())
    }
    
    fn round(self) -> Self {
        Value::new(self.get().round())
    }
    
    fn trunc(self) -> Self {
        Value::new(self.get().trunc())
    }
    
    fn fract(self) -> Self {
        Value::new(self.get().fract())
    }
    
    fn abs(self) -> Self {
        Value::new(self.get().abs())
    }
    
    fn signum(self) -> Self {
        Value::new(self.get().signum())
    }
    
    fn is_sign_positive(self) -> bool {
        self.get().is_sign_positive()
    }
    
    fn is_sign_negative(self) -> bool {
        self.get().is_sign_negative()
    }
    
    fn mul_add(self, a: Self, b: Self) -> Self {
        Value::new(self.get().mul_add(a.get(), b.get()))
    }
    
    fn recip(self) -> Self {
        Value::new(self.get().recip())
    }
    
    fn powi(self, n: i32) -> Self {
        Value::new(self.get().powi(n))
    }
    
    fn powf(self, n: Self) -> Self {
        Value::new(self.get().powf(n.get()))
    }
    
    fn sqrt(self) -> Self {
        Value::new(self.get().sqrt())
    }
    
    fn exp(self) -> Self {
        Value::new(self.get().exp())
    }
    
    fn exp2(self) -> Self {
        Value::new(self.get().exp2())
    }
    
    fn ln(self) -> Self {
        Value::new(self.get().ln())
    }
    
    fn log(self, base: Self) -> Self {
        Value::new(self.get().log(base.get()))
    }
    
    fn log2(self) -> Self {
        Value::new(self.get().log2())
    }
    
    fn log10(self) -> Self {
        Value::new(self.get().log10())
    }
    
    fn max(self, other: Self) -> Self {
        Value::new(self.get().max(other.get()))
    }
    
    fn min(self, other: Self) -> Self {
        Value::new(self.get().min(other.get()))
    }
    
    fn abs_sub(self, other: Self) -> Self {
        Value::new(self.get().abs_sub(other.get()))
    }
    
    fn cbrt(self) -> Self {
        Value::new(self.get().cbrt())
    }
    
    fn hypot(self, other: Self) -> Self {
        Value::new(self.get().hypot(other.get()))
    }
    
    fn sin(self) -> Self {
        Value::new(self.get().sin())
    }
    
    fn cos(self) -> Self {
        Value::new(self.get().cos())
    }
    
    fn tan(self) -> Self {
        Value::new(self.get().tan())
    }
    
    fn asin(self) -> Self {
        Value::new(self.get().asin())
    }
    
    fn acos(self) -> Self {
        Value::new(self.get().acos())
    }
    
    fn atan(self) -> Self {
        Value::new(self.get().atan())
    }
    
    fn atan2(self, other: Self) -> Self {
        Value::new(self.get().atan2(other.get()))
    }
    
    fn sin_cos(self) -> (Self, Self) {
        let (s, c) = self.get().sin_cos();
        (Value::new(s), Value::new(c))
    }
    
    fn exp_m1(self) -> Self {
        Value::new(self.get().exp_m1())
    }
    
    fn ln_1p(self) -> Self {
        Value::new(self.get().ln_1p())
    }
    
    fn sinh(self) -> Self {
        Value::new(self.get().sinh())
    }
    
    fn cosh(self) -> Self {
        Value::new(self.get().cosh())
    }
    
    fn tanh(self) -> Self {
        Value::new(self.get().tanh())
    }
    
    fn asinh(self) -> Self {
        Value::new(self.get().asinh())
    }
    
    fn acosh(self) -> Self {
        Value::new(self.get().acosh())
    }
    
    fn atanh(self) -> Self {
        Value::new(self.get().atanh())
    }
    
    fn integer_decode(self) -> (u64, i16, i8) {
        self.get().integer_decode()
    }
}
