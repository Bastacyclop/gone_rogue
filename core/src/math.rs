use cgmath::{Point2, Vector2};

use Float;

#[derive(Clone, Debug)]
pub struct Line<F: Float>(pub Point2<F>, pub Vector2<F>);
#[derive(Clone, Debug)]
pub struct Ray<F: Float>(pub Point2<F>, pub Vector2<F>);
#[derive(Clone, Debug)]
pub struct Segment<F: Float>(pub Point2<F>, pub Point2<F>);

pub trait LineIntersect<F: Float> {
    fn to_line(&self) -> Line<F>;
    fn check_parameter(t: F) -> bool;
}

impl<F: Float> LineIntersect<F> for Line<F> {
    fn to_line(&self) -> Line<F> {
        self.clone()
    }
    
    fn check_parameter(_: F) -> bool {
        true
    }
}

impl<F: Float> LineIntersect<F> for Ray<F> {
    fn to_line(&self) -> Line<F> {
        Line(self.0, self.1)
    }
    
    fn check_parameter(t: F) -> bool {
        // we need to go in the ray direction
        t >= F::zero()
    }
}

impl<F: Float> LineIntersect<F> for Segment<F> {
    fn to_line(&self) -> Line<F> {
        Line(self.0, self.1 - self.0)
    }
    
    fn check_parameter(t: F) -> bool {
        // we need to be inside the segment
        F::zero() <= t && t <= F::one()
    }
}

#[inline(always)]
fn line_intersection_u<F>(&Line(p_s, p_d): &Line<F>, &Line(q_s, q_d): &Line<F>) -> Option<F>
    where F: Float
{
    // p_s + t * p_d = q_s + u * q_d
    // ->
    // t = (q_s + u * q_d - p_s) / p_d
    // ->
    // p_d.y * (q_s.x + u * q_d.x - p_s.x)
    // = p_d.x * (q_s.y + u * q_d.y - p_s.y)
    // ->
    // p_d.y * q_s.x + p_d.y * u * q_d.x - p_d.y * p_s.x
    // = p_d.x * q_s.y + p_d.x * u * q_d.y - p_d.x * p_s.y
    // ->
    // p_d.y * u * q_d.x - p_d.x * u * q_d.y
    // = p_d.x * q_s.y - p_d.x * p_s.y - p_d.y * q_s.x + p_d.y * p_s.x
    // ->
    // u = (p_d.x (q_s.y - p_s.y) + p_d.y (p_s.x - q_s.x)) / (p_d.y * q_d.x - p_d.x * q_d.y)
    let u = (p_d.x * (q_s.y - p_s.y) + p_d.y * (p_s.x - q_s.x)) / (p_d.y * q_d.x - p_d.x * q_d.y);
    if u.is_normal() {
        Some(u)
    } else {
        None
    }
}

#[inline(always)]
fn line_point<F: Float>(&Line(s, d): &Line<F>, t: F) -> Point2<F> {
    s + d * t
}

#[inline(always)]
/// `p` is supposed to be on the line
fn line_parameter<F: Float>(&Line(s, d): &Line<F>, p: Point2<F>) -> F {
    (p.x - s.x) / d.x
}

pub fn intersection<P, Q, F>(p: &P, q: &Q) -> Option<Point2<F>>
    where P: LineIntersect<F>,
          Q: LineIntersect<F>,
          F: Float
{

    let p = p.to_line();
    let q = q.to_line();

    line_intersection_u(&p, &q).and_then(|u| {
        if !Q::check_parameter(u) {
            return None;
        }

        let i = line_point(&q, u);
        let t = line_parameter(&p, i);
        if !P::check_parameter(t) {
            return None;
        }

        Some(i)
    })
}
