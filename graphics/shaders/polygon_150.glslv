#version 150 core

in vec2 a_Position;
in uint a_Color;

uniform Locals {
    vec2 u_Scale;
    vec2 u_Translate;
};

out vec4 v_Color;

vec4 unpack_color(in uint);
float component_srgb_to_linear(in float f);
vec4 gamma_srgb_to_linear(in vec4 color);

void main() {
    gl_Position = vec4((a_Position + u_Translate) * u_Scale, 0.0, 1.0);
    v_Color = gamma_srgb_to_linear(unpack_color(a_Color));
}

vec4 unpack_color(in uint color) {
    const uint u8mask = 0x000000FFu;
    
    return vec4(float( a_Color >> 24),
                float((a_Color >> 16) & u8mask),
                float((a_Color >>  8) & u8mask),
                float( a_Color        & u8mask)) / 255.0;
}

float component_srgb_to_linear(in float f) {
    if (f <= 0.04045) {
        return f / 12.92;
    } else {
        return pow((f + 0.055) / 1.055, 2.4);
    }
}

vec4 gamma_srgb_to_linear(in vec4 color) {
    return vec4(component_srgb_to_linear(color.r),
                component_srgb_to_linear(color.g),
                component_srgb_to_linear(color.b),
                color.a);
}
