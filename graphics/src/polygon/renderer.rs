use {gfx, GfxResources, Backend};
use shader_version::{OpenGL, Shaders};
use shader_version::glsl::GLSL;

use camera::Camera;

pub const VERTEX_GLSL_150: &'static [u8] = include_bytes!("../../shaders/polygon_150.glslv");
pub const FRAGMENT_GLSL_150: &'static [u8] = include_bytes!("../../shaders/polygon_150.glslf");

gfx_defines! {
    vertex Vertex {
        position: [f32; 2] = "a_Position",
        color: u32 = "a_Color",
    }
    
    constant Locals {
        scale: [f32; 2] = "u_Scale",
        translate: [f32; 2] = "u_Translate",
    }

    pipeline pipe {
        vertex: gfx::VertexBuffer<Vertex> = (),
        locals: gfx::ConstantBuffer<Locals> = "Locals",
        out: gfx::RenderTarget<gfx::format::Srgba8> = "o_Color",
    }
}

pub const MAX_TRIANGLE_COUNT: usize = 1024;
pub const MAX_VERTEX_COUNT: usize = MAX_TRIANGLE_COUNT * 3;

pub type Triangle = [[f32; 2]; 3];

pub struct Renderer {
    buffer: gfx::handle::Buffer<GfxResources, Vertex>,
    offset: usize,
    pso: gfx::pso::PipelineState<GfxResources, pipe::Meta>,
    locals: gfx::handle::Buffer<GfxResources, Locals>,
}

impl Renderer {
    pub fn new(opengl: OpenGL, gfx: &mut Backend) -> Self {
        use gfx::traits::*;

        let glsl = opengl.to_glsl();

        let pso = gfx.factory
                     .create_pipeline_simple(Shaders::new()
                                                 .set(GLSL::V1_50, VERTEX_GLSL_150)
                                                 .get(glsl)
                                                 .unwrap(),
                                             Shaders::new()
                                                 .set(GLSL::V1_50, FRAGMENT_GLSL_150)
                                                 .get(glsl)
                                                 .unwrap(),
                                             pipe::new())
                     .unwrap();

        let buffer = gfx.factory
                        .create_buffer_dynamic(MAX_VERTEX_COUNT,
                                               gfx::BufferRole::Vertex,
                                               gfx::Bind::empty())
                        .expect("could not create `buffer`");
                        
        let locals = gfx.factory.create_constant_buffer(1);
        
        Renderer {
            buffer: buffer,
            offset: 0,
            pso: pso,
            locals: locals,
        }
    }

    pub fn prepare_render(&mut self, gfx: &mut Backend, camera: &Camera) {
        let scale = 2.0 / camera.size.get() as f32;
        gfx.encoder.update_constant_buffer(&self.locals, &Locals {
            scale: [scale, scale],
            translate: [-camera.position.x.get() as f32, -camera.position.y.get() as f32]
        });
    }

    pub fn finalize_render(&mut self, gfx: &mut Backend) {
        if self.offset > 0 {
            self.flush(gfx);
        }
    }
    
    pub fn render<S>(&mut self, gfx: &mut Backend, color: u32, stream: S)
        where S: FnOnce(&mut FnMut(&Triangle))
    {
        use gfx::traits::*;
        // let mut factory = gfx.factory.clone();
        // let mut w = factory.map_buffer_writable(&self.buffer);

        stream(&mut |triangle: &[[f32; 2]; 3]| {
            if self.offset + 3 > MAX_VERTEX_COUNT {
                self.flush(gfx);
            }

            for i in 0..3 {
                // w.set(self.offset + i, Vertex { position: triangle[i], color: color });
                gfx.encoder
                   .update_buffer(&self.buffer,
                                  &[Vertex {
                                        position: triangle[i],
                                        color: color,
                                    }],
                                  self.offset + i)
                   .unwrap();
            }

            self.offset += 3;
        });
    }
    
    fn flush(&mut self, gfx: &mut Backend) {
        let data = pipe::Data {
            vertex: self.buffer.clone(),
            locals: self.locals.clone(),
            out: gfx.output_color.clone(),
        };

        let slice = gfx::Slice {
            instances: None,
            start: 0,
            end: self.offset as u32,
            buffer: gfx::IndexBuffer::Auto,
            base_vertex: 0,
        };

        gfx.encoder.draw(&slice, &self.pso, &data);
        gfx.encoder.flush(&mut gfx.device);

        self.offset = 0;
    }
}
