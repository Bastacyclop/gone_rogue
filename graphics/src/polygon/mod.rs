pub mod renderer;
pub use self::renderer::{Renderer, Triangle};

use graphics::triangulation;

pub fn scale_triangle(t: Triangle, s: f32) -> Triangle {
    [[t[0][0] * s, t[0][1] * s], [t[1][0] * s, t[1][1] * s], [t[2][0] * s, t[2][1] * s]]
}

pub fn translate_triangle(t: Triangle, v: [f32; 2]) -> Triangle {
    [[t[0][0] + v[0], t[0][1] + v[1]],
     [t[1][0] + v[0], t[1][1] + v[1]],
     [t[2][0] + v[0], t[2][1] + v[1]]]
}

pub fn transform_triangle(t: Triangle, scale: f32, translate: [f32; 2]) -> Triangle {
    translate_triangle(scale_triangle(t, scale), translate)
}

pub fn polygon<E, F>(transform: [[f64; 3]; 2], polygon: E, f: F)
    where E: FnMut() -> Option<[f64; 2]>,
          F: FnMut(&Triangle)
{
    let mut sc = StreamConverter::new(f);

    triangulation::stream_polygon_tri_list(transform, polygon, |s| sc.step(s));
}

pub fn ellipse<F>(rectangle: [f64; 4], transform: [[f64; 3]; 2], resolution_cap: u32, f: F)
    where F: FnMut(&Triangle)
{
    let mut sc = StreamConverter::new(f);

    triangulation::with_ellipse_tri_list(resolution_cap, transform, rectangle, |s| sc.step(s));
}

pub fn ellipse_border<F>(rectangle: [f64; 4],
                         radius: f64,
                         transform: [[f64; 3]; 2],
                         resolution_cap: u32,
                         f: F)
    where F: FnMut(&Triangle)
{
    let mut sc = StreamConverter::new(f);

    triangulation::with_ellipse_border_tri_list(resolution_cap,
                                                transform,
                                                rectangle,
                                                radius,
                                                |s| sc.step(s));
}

pub fn line<F>(a: [f64; 2],
               b: [f64; 2],
               radius: f64,
               transform: [[f64; 3]; 2],
               resolution_cap: u32,
               f: F)
    where F: FnMut(&Triangle)
{
    let mut sc = StreamConverter::new(f);

    triangulation::with_round_border_line_tri_list(resolution_cap,
                                                   transform,
                                                   [a[0], a[1], b[0], b[1]],
                                                   radius,
                                                   |s| sc.step(s));
}

pub fn rectangle<F>(rectangle: [f64; 4], transform: [[f64; 3]; 2], f: F)
    where F: FnMut(&Triangle)
{
    let mut sc = StreamConverter::new(f);

    sc.step(&triangulation::rect_tri_list_xy(transform, rectangle));
}

pub fn rectangle_border<F>(rectangle: [f64; 4], border_radius: f64, transform: [[f64; 3]; 2], f: F)
    where F: FnMut(&Triangle)
{
    let mut sc = StreamConverter::new(f);

    sc.step(&triangulation::rect_border_tri_list_xy(transform, rectangle, border_radius));
}

pub fn round_rectangle<F>(rectangle: [f64; 4],
                          round_radius: f64,
                          transform: [[f64; 3]; 2],
                          resolution_cap: u32,
                          f: F)
    where F: FnMut(&Triangle)
{
    let mut sc = StreamConverter::new(f);

    triangulation::with_round_rectangle_tri_list(resolution_cap,
                                                 transform,
                                                 rectangle,
                                                 round_radius,
                                                 |s| sc.step(s));
}

pub fn round_rectangle_border<F>(rectangle: [f64; 4],
                                 round_radius: f64,
                                 border_radius: f64,
                                 transform: [[f64; 3]; 2],
                                 resolution_cap: u32,
                                 f: F)
    where F: FnMut(&Triangle)
{
    let mut sc = StreamConverter::new(f);

    triangulation::with_round_rectangle_border_tri_list(resolution_cap,
                                                        transform,
                                                        rectangle,
                                                        round_radius,
                                                        border_radius,
                                                        |s| sc.step(s));
}

struct StreamConverter<F> {
    triangle: [[f32; 2]; 3],
    i: u8,
    f: F,
}

impl<F> StreamConverter<F>
    where F: FnMut(&Triangle)
{
    fn new(f: F) -> Self {
        StreamConverter {
            triangle: [[0.0, 0.0]; 3],
            i: 0,
            f: f,
        }
    }

    fn step(&mut self, stream: &[f32]) {
        for vertice in stream.chunks(2) {
            self.triangle[self.i as usize] = [vertice[0], vertice[1]];
            self.i += 1;

            if self.i == 3 {
                (self.f)(&self.triangle);
                self.i = 0;
            }
        }

        debug_assert!(self.i == 0);
    }
}
