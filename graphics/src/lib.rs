#[macro_use]
extern crate gfx;
extern crate gfx_core;
extern crate gfx_device_gl;
extern crate piston;
extern crate graphics;
extern crate gfx_graphics;
extern crate glutin_window;
extern crate shader_version;
extern crate cgmath;

extern crate core;

pub mod polygon;
pub mod camera;
pub use camera::Camera;

use std::path::Path;
use glutin_window::GlutinWindow;
use shader_version::OpenGL;
pub use piston::window::*;
pub use piston::input::*;
pub use piston::event_loop::*;
pub use gfx_graphics::{GlyphError, TextureSettings, Flip};

pub type GfxResources = gfx_device_gl::Resources;
pub type GfxCommandBuffer = gfx_device_gl::CommandBuffer;
type GfxEncoder = gfx::Encoder<GfxResources, GfxCommandBuffer>;
type GfxDevice = gfx_device_gl::Device;
pub type GfxFactory = gfx_device_gl::Factory;
pub type GfxGraphics<'a> = gfx_graphics::GfxGraphics<'a, GfxResources, GfxCommandBuffer>;
type GfxOutputColor = gfx::handle::RenderTargetView<GfxResources, gfx::format::Srgba8>;
type GfxOutputStencil = gfx::handle::DepthStencilView<GfxResources, gfx::format::DepthStencil>;
pub type Glyphs = gfx_graphics::GlyphCache<GfxResources, GfxFactory>;
pub type Texture = gfx_graphics::Texture<GfxResources>;

pub const TRANSFORM_IDENTITY: [[f64; 3]; 2] = [
    [1., 0., 0.],
    [0., 1., 0.],
];

pub fn pack_color((r, g, b, a): (u8, u8, u8, u8)) -> u32 {
    (r as u32) << 24 | (g as u32) << 16 | (b as u32) << 8 | a as u32
}

pub struct Graphics {
    window: GlutinWindow,
    events: WindowEvents,
    polygon_renderer: polygon::Renderer,
    gfx: Backend,
}

pub struct Backend {
    encoder: GfxEncoder,
    device: GfxDevice,
    factory: GfxFactory,
    output_color: GfxOutputColor,
    output_stencil: GfxOutputStencil,
}

impl Graphics {
    pub fn new(title: &str, w: u32, h: u32) -> Graphics {
        use piston::window::OpenGLWindow;

        let opengl = OpenGL::V3_2; // FIXME: 2.1 not working

        let mut window: GlutinWindow = WindowSettings::new(title, [w, h])
                                           .opengl(opengl)
                                           .exit_on_esc(true)
                                           .build()
                                           .unwrap();
        let events = window.events().swap_buffers(false);

        let (device, mut factory) = gfx_device_gl::create(|s| {
            window.get_proc_address(s) as *const _
        });

        let (output_color, output_stencil) = {
            let aa = 1u8 as gfx::tex::NumSamples;
            let draw_size = window.draw_size();
            let dim = (draw_size.width as u16,
                       draw_size.height as u16,
                       1,
                       aa.into());
            create_main_targets(dim)
        };

        let mut gfx = Backend {
            encoder: factory.create_command_buffer().into(),
            device: device,
            factory: factory,
            output_color: output_color,
            output_stencil: output_stencil,
        };

        Graphics {
            window: window,
            events: events,
            polygon_renderer: polygon::Renderer::new(opengl, &mut gfx),
            gfx: gfx,
        }
    }

    pub fn load_texture<P: AsRef<Path>>(&mut self, path: P) -> Option<Texture> {
        Texture::from_path(&mut self.gfx.factory,
                           path,
                           gfx_graphics::Flip::None,
                           &gfx_graphics::TextureSettings::new())
            .ok()
    }

    pub fn load_glyphs<P: AsRef<Path>>(&mut self, path: P) -> Option<Glyphs> {
        Glyphs::new(path, self.gfx.factory.clone()).ok()
    }

    pub fn clear(&mut self, color: [f32; 4], camera: &Camera) {
        let color = graphics::color::gamma_srgb_to_linear(color);
        self.gfx.encoder.clear(&self.gfx.output_color, color);
        self.polygon_renderer.prepare_render(&mut self.gfx, &camera);
    }

    pub fn render<S>(&mut self, color: u32, stream: S)
        where S: FnOnce(&mut FnMut(&polygon::Triangle))
    {
        self.polygon_renderer.render(&mut self.gfx, color, stream);
    }

    pub fn render_slice(&mut self, color: u32, slice: &[polygon::Triangle]) {
        self.render(color, |render_triangle| {
            for t in slice {
                render_triangle(t);
            }
        })
    }

    pub fn flush(&mut self) {
        use gfx::Device;
        
        self.polygon_renderer.finalize_render(&mut self.gfx);
        self.window.swap_buffers();
        self.gfx.device.cleanup();
    }
    
    pub fn resize(&mut self) {
        use gfx_core::factory::Typed;
        
        // Check whether window has resized and update the output.
        let dim = self.gfx.output_color.raw().get_dimensions();
        let (w, h) = (dim.0, dim.1);
        let draw_size = self.window.draw_size();
        if w != draw_size.width as u16 || h != draw_size.height as u16 {
            let dim = (draw_size.width as u16,
                       draw_size.height as u16,
                       dim.2,
                       dim.3);
            let (output_color, output_stencil) = create_main_targets(dim);
            self.gfx.output_color = output_color;
            self.gfx.output_stencil = output_stencil;
        }
    }
    
    pub fn next_event(&mut self) -> Option<Event<<GlutinWindow as Window>::Event>> {
        self.events.next(&mut self.window)
    }
    
    pub fn set_ups(&mut self, frames: u64) {
        self.events.set_ups(frames)
    }

    pub fn set_max_fps(&mut self, frames: u64) {
        self.events.set_max_fps(frames)
    }
}

fn create_main_targets(dim: gfx::tex::Dimensions) -> (GfxOutputColor, GfxOutputStencil) {
    use gfx_core::factory::Typed;
    use gfx::format::{DepthStencil, Format, Formatted, Srgba8};

    let color_format: Format = <Srgba8 as Formatted>::get_format();
    let depth_format: Format =
        <DepthStencil as Formatted>::get_format();
    let (output_color, output_stencil) = gfx_device_gl::create_main_targets_raw(dim,
                                                                                color_format.0,
                                                                                depth_format.0);
    let output_color = Typed::new(output_color);
    let output_stencil = Typed::new(output_stencil);
    (output_color, output_stencil)
}

impl Window for Graphics {
    type Event = <GlutinWindow as Window>::Event;

    fn should_close(&self) -> bool {
        self.window.should_close()
    }
    fn set_should_close(&mut self, value: bool) {
        self.window.set_should_close(value)
    }
    fn size(&self) -> Size {
        self.window.size()
    }
    fn draw_size(&self) -> Size {
        self.window.draw_size()
    }
    fn swap_buffers(&mut self) {
        self.window.swap_buffers()
    }
    fn poll_event(&mut self) -> Option<Self::Event> {
        <GlutinWindow as Window>::poll_event(&mut self.window)
    }
}

impl AdvancedWindow for Graphics {
    fn get_title(&self) -> String {
        self.window.get_title()
    }
    fn set_title(&mut self, title: String) {
        self.window.set_title(title)
    }
    fn get_exit_on_esc(&self) -> bool {
        self.window.get_exit_on_esc()
    }
    fn set_exit_on_esc(&mut self, value: bool) {
        self.window.set_exit_on_esc(value)
    }
    fn set_capture_cursor(&mut self, value: bool) {
        self.window.set_capture_cursor(value)
    }
}
