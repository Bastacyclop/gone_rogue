use cgmath::Point2;

use core::unit::*;

pub struct Camera {
    pub size: Meters,
    pub position: Point2<Meters>
}

impl Camera {
    pub fn new() -> Self {
        Camera {
            size: meters(20.0),
            position: Point2::new(meters(0.), meters(0.))
        }
    }
    
    pub fn window_to_world(&self, p: &Point2<f64>) -> Point2<Meters> {
        Point2 {
            x: meters((p.x * 0.5 * self.size.get()) + self.position.x.get()),
            y: meters((p.y * 0.5 * self.size.get()) + self.position.y.get()),
        }
    }
}
