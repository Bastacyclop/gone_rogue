## Gone Rogue

### TODO

- [ ] gameplay
    - [ ] exploration
    - [ ] fight
- [ ] world building
    - [ ] random sites
    - [x] voronoi graph
        - [x] Fortune's algorithm
        - [x] Lloyd algorithm
    - [ ] biomes
    - [ ] elevation, rivers, montains, moisture, ...
    - [ ] noise and transitions
    - [ ] details (clouds, sky, shapes, fauna, flora, ...)
- [ ] physics
    - [ ] interpolation
- [ ] audio
- [ ] graphics
    - [ ] better polygon rendering process
    - [ ] GUI
- [ ] configuration
- [ ] path finding
