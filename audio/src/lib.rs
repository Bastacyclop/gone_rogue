extern crate rodio;

use std::path::Path;
use rodio::Endpoint;

pub struct Audio {
    endpoint: Endpoint,
}

impl Audio {
    pub fn new() -> Audio {
        Audio { endpoint: rodio::get_default_endpoint().unwrap() }
    }

    pub fn play<P: AsRef<Path>>(&self, path: P) {
        use std::fs::File;
        use std::io::BufReader;

        let sink = rodio::Sink::new(&self.endpoint);

        let file = File::open(path).unwrap();
        let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
        sink.append(source);
        sink.detach();
    }
}
