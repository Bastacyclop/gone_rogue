use cgmath::Point2;

use core::Float;
use voronoi::{SiteId, Site};

pub struct Graph<F: Float> {
    pub cells: Vec<Cell>,
    pub nodes: Vec<Node<F>>,
    pub edges: Vec<Edge>,
}

#[derive(Debug)]
pub struct Cell {
    pub neighbors: Vec<CellId>,
    pub nodes: Vec<NodeId>,
}

#[derive(Debug)]
pub struct Node<F: Float> {
    pub point: Point2<F>,
    pub edges: Vec<EdgeId>,
}

#[derive(Debug)]
pub struct Edge {
    pub u: NodeId,
    pub v: NodeId,
}

impl<F: Float> Graph<F> {
    pub fn new() -> Self {
        Graph {
            cells: Vec::new(),
            nodes: Vec::new(),
            edges: Vec::new(),
        }
    }

    pub fn clear(&mut self) {
        self.cells.clear();
        self.nodes.clear();
        self.edges.clear();
    }

    pub fn add<E: Element<F>>(&mut self, e: E) -> E::Id {
        let vec = E::get_container_mut(self);
        let id = E::Id::from_index(vec.len());
        vec.push(e);
        id
    }

    pub fn link_edge(&mut self, edge: Edge) -> EdgeId {
        let u = edge.u;
        let v = edge.v;

        let id = self.add(edge);
        self.get_mut(u).edges.push(id);
        self.get_mut(v).edges.push(id);
        id
    }

    pub fn delaunay_edge(&mut self, a: SiteId, b: SiteId) {
        let (a, b) = (CellId(a.0), CellId(b.0));
        self.get_mut(a).neighbors.push(b);
        self.get_mut(b).neighbors.push(a);
    }

    pub fn get<I: ElementId<F>>(&self, id: I) -> &I::Element {
        &I::Element::get_container(self)[id.to_index()]
    }

    pub fn get_mut<I: ElementId<F>>(&mut self, id: I) -> &mut I::Element {
        &mut I::Element::get_container_mut(self)[id.to_index()]
    }

    pub fn sort_cell_nodes_ccw(&mut self, sites: &[Site<F>]) {
        let &mut Graph { ref mut cells, ref nodes, .. } = self;

        for (c, cell) in cells.iter_mut().enumerate() {
            let site = sites[c];
            let angle = |p: Point2<F>| {
                let x = p.x - site.x;
                let y = p.y - site.y;
                y.atan2(x)
            };

            cell.nodes.sort_by(|&a, &b| {
                if angle(nodes[a.0].point) < angle(nodes[b.0].point) {
                    ::std::cmp::Ordering::Less
                } else {
                    ::std::cmp::Ordering::Greater
                }
            });
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct CellId(pub usize);
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct NodeId(pub usize);
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct EdgeId(pub usize);

impl Cell {
    pub fn new() -> Self {
        Cell {
            neighbors: Vec::new(),
            nodes: Vec::new(),
        }
    }
}

impl<F: Float> Node<F> {
    pub fn new(point: Point2<F>) -> Self {
        Node {
            point: point,
            edges: Vec::new(),
        }
    }
}

impl Edge {
    pub fn neighbor(&self, node: NodeId) -> NodeId {
        if self.u == node {
            self.v
        } else if self.v == node {
            self.u
        } else {
            panic!("the given node does not belong to this edge");
        }
    }
}

pub trait Element<F: Float>: Sized {
    type Id: ElementId<F>;

    fn get_container(graph: &Graph<F>) -> &Vec<Self>;
    fn get_container_mut(graph: &mut Graph<F>) -> &mut Vec<Self>;
}

pub trait ElementId<F: Float> {
    type Element: Element<F>;

    fn from_index(index: usize) -> Self;
    fn to_index(self) -> usize;
}

impl<F: Float> Element<F> for Cell {
    type Id = CellId;

    fn get_container(graph: &Graph<F>) -> &Vec<Self> {
        &graph.cells
    }

    fn get_container_mut(graph: &mut Graph<F>) -> &mut Vec<Self> {
        &mut graph.cells
    }
}

impl<F: Float> ElementId<F> for CellId {
    type Element = Cell;

    fn from_index(index: usize) -> Self {
        CellId(index)
    }

    fn to_index(self) -> usize {
        self.0
    }
}

impl<F: Float> Element<F> for Node<F> {
    type Id = NodeId;

    fn get_container(graph: &Graph<F>) -> &Vec<Self> {
        &graph.nodes
    }

    fn get_container_mut(graph: &mut Graph<F>) -> &mut Vec<Self> {
        &mut graph.nodes
    }
}

impl<F: Float> ElementId<F> for NodeId {
    type Element = Node<F>;

    fn from_index(index: usize) -> Self {
        NodeId(index)
    }

    fn to_index(self) -> usize {
        self.0
    }
}

impl<F: Float> Element<F> for Edge {
    type Id = EdgeId;

    fn get_container(graph: &Graph<F>) -> &Vec<Self> {
        &graph.edges
    }

    fn get_container_mut(graph: &mut Graph<F>) -> &mut Vec<Self> {
        &mut graph.edges
    }
}

impl<F: Float> ElementId<F> for EdgeId {
    type Element = Edge;

    fn from_index(index: usize) -> Self {
        EdgeId(index)
    }

    fn to_index(self) -> usize {
        self.0
    }
}
