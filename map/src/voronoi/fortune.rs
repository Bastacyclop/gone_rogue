//! Fortune's algorithm.
//!
//! This implementation is based on
//! [this document](http://skynet.ie/~sos/mapviewer/docs/Voronoi_Diagram_Notes_1.pdf).
//!
//! Currently sites with same `y` coordinate are not supported.
//! What happens if there are duplicate sites ?
//! `BinaryHeap` requests `Ord` and so it is implemented while it shouldn't.

use std::mem;
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use cgmath::{Point2, Vector2};
use bit_vec::BitVec;

use core::Float;
use core::math::{self, intersection};
use voronoi::{Site, SiteId, Graph, Cell, Node, Edge, CellId, NodeId, EdgeId};

pub struct Data<F: Float> {
    events: BinaryHeap<Event<F>>,
    valid_circles: ValidCircles,
    beach_line: BeachLine<F>,
    border: Border<F>,
    border_nodes: BinaryHeap<BorderNode<F>>,
    sweep_y: F,
}

impl<F: Float> Data<F> {
    pub fn new() -> Self {
        Data {
            events: BinaryHeap::new(),
            valid_circles: ValidCircles::new(),
            beach_line: BeachLine::new(),
            border: Border::from_rect(&[F::zero(), F::zero(), F::zero(), F::zero()]),
            border_nodes: BinaryHeap::new(),
            sweep_y: F::zero(), // will be overriden by first event
        }
    }

    fn reset(&mut self, rect: &[F; 4]) {
        self.events.clear();
        self.valid_circles.clear();
        self.beach_line.clear();
        self.border = Border::from_rect(rect);
        self.border_nodes.clear();
    }

    pub fn compute(&mut self, sites: &[Site<F>], rect: &[F; 4], graph: &mut Graph<F>) {
        graph.clear();
        self.reset(rect);

        for (i, site) in sites.iter().enumerate() {
            graph.add(Cell::new());

            self.events.push(Event {
                actual: ActualEvent::Site(SiteId(i)),
                y: site.y,
            });
        }

        while let Some(e) = self.events.pop() {
            self.sweep_y = e.y;

            match e.actual {
                ActualEvent::Site(index) => self.site_event(index, sites, graph),
                ActualEvent::Vertex { circle_id, arc, center } => {
                    self.valid_circles.release_id(circle_id);
                    if self.valid_circles.contains(circle_id) {
                        self.vertex_event(center, arc, sites, graph);
                    }
                }
            }
        }

        self.clip_rays(sites, graph);
        self.close_edges(graph);
    }

    fn site_event(&mut self, site: SiteId, sites: &[Site<F>], graph: &mut Graph<F>) {

        match self.beach_line.root() {
            None => {
                let a = Arc {
                    circle_id: None,
                    site: site,
                    parent: None,
                };
                self.beach_line.nodes.push(BeachNode::Arc(a));
            }
            Some(_) => {
                let s = sites[site.0];
                let old_arc = self.beach_line.get_arc_by_x(s.x, self.sweep_y, sites);
                let oa = self.beach_line.get_arc(old_arc).clone();

                let oas = sites[oa.site.0];
                let ray_start = Point2::new(s.x, self.get_arc_y_from_x(oas, s.x));
                let (left_ray, right_ray) = self.spawn_rays(ray_start, oa.site, site, sites, graph);
                graph.delaunay_edge(oa.site, site);

                let breakpoint = unsafe { self.beach_line.add_node(mem::uninitialized()) };

                let left_arc = self.beach_line.add_node(BeachNode::Arc(Arc {
                    circle_id: None,
                    site: oa.site,
                    parent: Some(breakpoint),
                }));

                let new_arc = self.beach_line.add_node(BeachNode::Arc(Arc {
                    circle_id: None,
                    site: site,
                    parent: Some(breakpoint),
                }));

                let right_arc = self.beach_line.add_node(BeachNode::Arc(Arc {
                    circle_id: None,
                    site: oa.site,
                    parent: Some(old_arc),
                }));

                self.beach_line.nodes[breakpoint] = BeachNode::Breakpoint(Breakpoint {
                    ray: left_ray,
                    parent: Some(old_arc),
                    left: left_arc,
                    right: new_arc,
                });

                self.beach_line.nodes[old_arc] = BeachNode::Breakpoint(Breakpoint {
                    ray: right_ray,
                    parent: oa.parent,
                    left: breakpoint,
                    right: right_arc,
                });

                self.valid_circles.might_remove(oa.circle_id);

                self.check_circle(left_arc, sites);
                self.check_circle(right_arc, sites);
            }
        }
    }

    fn vertex_event(&mut self,
                    center: Point2<F>,
                    arc: usize,
                    sites: &[Site<F>],
                    graph: &mut Graph<F>) {

        let left_breakpoint = self.beach_line.get_closest_left_parent(arc).unwrap();
        let right_breakpoint = self.beach_line.get_closest_right_parent(arc).unwrap();

        let left_arc = self.beach_line.get_closest_left_child_arc(left_breakpoint);
        let right_arc = self.beach_line.get_closest_right_child_arc(right_breakpoint);

        let new_ray = {
            // TODO clean this up
            let a = self.beach_line.get_arc(arc);
            let la = self.beach_line.get_arc(left_arc);
            let ra = self.beach_line.get_arc(right_arc);
            let direction = Ray::direction(sites[ra.site.0], sites[la.site.0]);

            graph.delaunay_edge(la.site, ra.site);

            let lb = self.beach_line.get_breakpoint(left_breakpoint);
            let rb = self.beach_line.get_breakpoint(right_breakpoint);

            let ray_kind = if self.border.inside(&center) {
                let node = graph.add(Node::new(center));
                Self::bind_cell_node(node, la.site, graph);
                Self::bind_cell_node(node, a.site, graph);
                Self::bind_cell_node(node, ra.site, graph);

                lb.ray.bind(node, graph, &mut self.border_nodes);
                rb.ray.bind(node, graph, &mut self.border_nodes);

                let edge = graph.add(Edge { u: node, ..dangling_edge() });
                graph.get_mut(node).edges.push(edge);

                RayKind::FromInside(edge, EdgePart::V)
            } else {
                match lb.ray.kind {
                    RayKind::FromInside(edge, part) => {
                        let (side, point) = self.border.intersection(&lb.ray.to_math()).unwrap();
                        let border_node = graph.add(Node::new(point));
                        Self::bind_cell_node(border_node, la.site, graph);
                        Self::bind_cell_node(border_node, a.site, graph);

                        self.border_nodes.push(BorderNode {
                            side: side,
                            point: point,
                            node: border_node,
                        });

                        Self::bind_inside_ray(edge, part, border_node, graph);
                    }
                    _ => {}
                }
                match rb.ray.kind {
                    RayKind::FromInside(edge, part) => {
                        let (side, point) = self.border.intersection(&rb.ray.to_math()).unwrap();
                        let border_node = graph.add(Node::new(point));
                        Self::bind_cell_node(border_node, a.site, graph);
                        Self::bind_cell_node(border_node, ra.site, graph);

                        self.border_nodes.push(BorderNode {
                            side: side,
                            point: point,
                            node: border_node,
                        });

                        Self::bind_inside_ray(edge, part, border_node, graph);
                    }
                    _ => {}
                }
                if let Some((side, point)) = self.border
                                                 .intersection(&math::Ray(center, direction)) {
                    RayKind::FromOutside(side, point, la.site, ra.site)
                } else {
                    RayKind::GoingAway
                }
            };

            Ray {
                start: center,
                direction: direction,
                kind: ray_kind,
            }
        };

        let parent_breakpoint = self.beach_line.nodes[arc].parent().unwrap();
        let higher_breakpoint = if parent_breakpoint == left_breakpoint {
            right_breakpoint
        } else {
            left_breakpoint
        };

        self.beach_line.get_breakpoint_mut(higher_breakpoint).ray = new_ray;

        {
            let grand_parent_breakpoint;
            let remains = {
                let pb = self.beach_line.get_breakpoint(parent_breakpoint);
                grand_parent_breakpoint = pb.parent.unwrap();
                if pb.left == arc {
                    pb.right
                } else {
                    pb.left
                }
            };

            {
                let gpb = self.beach_line.get_breakpoint_mut(grand_parent_breakpoint);
                if gpb.left == parent_breakpoint {
                    gpb.left = remains;
                } else {
                    gpb.right = remains;
                }
            }

            let r = &mut self.beach_line.nodes[remains];
            r.set_parent(Some(grand_parent_breakpoint));
        }

        {
            let la = self.beach_line.get_arc_mut(left_arc);
            self.valid_circles.might_remove(la.circle_id.take());
        }
        {
            let ra = self.beach_line.get_arc_mut(right_arc);
            self.valid_circles.might_remove(ra.circle_id.take());
        }

        self.beach_line.free_node(arc);
        self.beach_line.free_node(parent_breakpoint);

        self.check_circle(left_arc, sites);
        self.check_circle(right_arc, sites);
    }

    // if there was already a circle associated to `arc` it might be overriden,
    // so you need to ensure it does not happen by removing it from the valid circles
    // before this call, if necessary
    fn check_circle(&mut self, arc: usize, sites: &[Site<F>]) {
        let left_breakpoint = match self.beach_line.get_closest_left_parent(arc) {
            Some(p) => p,
            None => return,
        };

        let right_breakpoint = match self.beach_line.get_closest_right_parent(arc) {
            Some(p) => p,
            None => return,
        };

        let mut circle_id = None;

        {
            let a = self.beach_line.get_arc(arc);
            let lb = self.beach_line.get_breakpoint(left_breakpoint);
            let rb = self.beach_line.get_breakpoint(right_breakpoint);

            if let Some(center) = intersection(&lb.ray.to_math(), &rb.ray.to_math()) {
                let p = sites[a.site.0];
                let dx = p.x - center.x;
                let dy = p.y - center.y;

                let d = ((dx * dx) + (dy * dy)).sqrt();

                let circle_lower_endpoint = center.y - d;
                if circle_lower_endpoint < self.sweep_y {
                    let id = self.valid_circles.insert();
                    let e = Event {
                        actual: ActualEvent::Vertex {
                            circle_id: id,
                            arc: arc,
                            center: center,
                        },
                        y: circle_lower_endpoint,
                    };
                    self.events.push(e);
                    circle_id = Some(id);
                }
            }
        };

        let a = self.beach_line.get_arc_mut(arc);
        a.circle_id = circle_id;
    }

    fn get_arc_y_from_x(&self, site: Point2<F>, x: F) -> F {
        let two = F::from(2.).unwrap();
        let four = F::from(4.).unwrap();
        
        let dp = two * (site.y - self.sweep_y);
        let a1 = F::one() / dp;
        let b1 = -two * site.x / dp;
        let c1 = self.sweep_y + dp / four + site.x * site.x / dp;

        a1 * x * x + b1 * x + c1
    }

    fn clip_rays(&mut self, sites: &[Site<F>], graph: &mut Graph<F>) {
        self.beach_line.root().map(|r| self.clip_rays_rec(r, sites, graph));
    }

    fn clip_rays_rec(&mut self, node: usize, sites: &[Site<F>], graph: &mut Graph<F>) {
        let (l, r) = match &self.beach_line.nodes[node] {
            &BeachNode::Arc(_) => return,
            &BeachNode::Breakpoint(ref b) => {
                match b.ray.kind {
                    RayKind::GoingAway => {}
                    _ => {
                        let left_arc = self.beach_line.get_closest_left_child_arc(node);
                        let right_arc = self.beach_line.get_closest_right_child_arc(node);
                        let la = self.beach_line.get_arc(left_arc);
                        let ra = self.beach_line.get_arc(right_arc);

                        let (side, point) = self.border.intersection(&b.ray.to_math()).unwrap();
                        let border_node = graph.add(Node::new(point));
                        Self::bind_cell_node(border_node, la.site, graph);
                        Self::bind_cell_node(border_node, ra.site, graph);

                        self.border_nodes.push(BorderNode {
                            side: side,
                            point: point,
                            node: border_node,
                        });

                        b.ray.bind(border_node, graph, &mut self.border_nodes);
                    }
                }

                (b.left, b.right)
            }
        };

        self.clip_rays_rec(l, sites, graph);
        self.clip_rays_rec(r, sites, graph);
    }

    fn close_edges(&mut self, graph: &mut Graph<F>) {
        if let Some(first) = self.border_nodes.pop() {
            let mut a = first.clone();
            while let Some(b) = self.border_nodes.pop() {
                graph.link_edge(Edge {
                    u: a.node,
                    v: b.node,
                });
                a = b;
            }

            graph.link_edge(Edge {
                u: a.node,
                v: first.node,
            });
        }
    }

    fn spawn_rays(&self,
                  start: Point2<F>,
                  top: SiteId,
                  bottom: SiteId,
                  sites: &[Site<F>],
                  graph: &mut Graph<F>)
                  -> (Ray<F>, Ray<F>) {
        let left = Ray::direction(sites[bottom.0], sites[top.0]);
        let right = -left;

        let (l_kind, r_kind) = if self.border.inside(&start) {
            let edge = graph.add(dangling_edge());
            (RayKind::FromInside(edge, EdgePart::U),
             RayKind::FromInside(edge, EdgePart::V))
        } else {
            if let Some((side, point)) = self.border.intersection(&math::Ray(start, left)) {
                (RayKind::FromOutside(side, point, top, bottom),
                 RayKind::GoingAway)
            } else if let Some((side, point)) = self.border.intersection(&math::Ray(start, right)) {
                (RayKind::GoingAway,
                 RayKind::FromOutside(side, point, top, bottom))
            } else {
                (RayKind::GoingAway, RayKind::GoingAway)
            }
        };

        (Ray {
            start: start,
            direction: left,
            kind: l_kind,
        },
         Ray {
            start: start,
            direction: right,
            kind: r_kind,
        })
    }
    
    fn bind_inside_ray(edge: EdgeId, part: EdgePart, node: NodeId, graph: &mut Graph<F>) {
        match part {
            EdgePart::U => graph.get_mut(edge).u = node,
            EdgePart::V => graph.get_mut(edge).v = node,
        }

        graph.get_mut(node).edges.push(edge);
    }

    fn bind_cell_node(node: NodeId, site: SiteId, graph: &mut Graph<F>) {
        graph.get_mut(CellId(site.0)).nodes.push(node);
    }
}

#[inline(always)]
fn dangling_edge() -> Edge {
    Edge {
        u: NodeId(0),
        v: NodeId(0),
    }
}

#[derive(Clone, Debug)]
struct BorderNode<F> {
    side: BorderSide,
    point: Point2<F>,
    node: NodeId,
}

impl<F: Float> PartialEq for BorderNode<F> {
    fn eq(&self, other: &Self) -> bool {
        self.node == other.node
    }
}

impl<F: Float> Eq for BorderNode<F> {}

impl<F: Float> PartialOrd for BorderNode<F> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        use self::BorderSide::*;

        if self.side < other.side {
            Some(Ordering::Less)
        } else if self.side > other.side {
            Some(Ordering::Greater)
        } else {
            match self.side {
                Top => other.point.x.partial_cmp(&self.point.x),
                Left => other.point.y.partial_cmp(&self.point.y),
                Bottom => self.point.x.partial_cmp(&other.point.x),
                Right => self.point.y.partial_cmp(&other.point.y),
            }
        }
    }
}

impl<F: Float> Ord for BorderNode<F> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(&other).expect("fortune::LimitIntersection Ord panic")
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
enum BorderSide {
    Top,
    Left,
    Bottom,
    Right,
}

#[derive(Debug)]
struct Event<F: Float> {
    actual: ActualEvent<F>,
    y: F,
}

#[derive(Debug)]
enum ActualEvent<F: Float> {
    Site(SiteId),
    Vertex {
        circle_id: CircleId,
        arc: usize,
        center: Point2<F>,
    },
}

impl<F: Float> PartialEq for Event<F> {
    fn eq(&self, other: &Self) -> bool {
        self.y == other.y
    }
}

impl<F: Float> Eq for Event<F> {}

impl<F: Float> PartialOrd for Event<F> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.y.partial_cmp(&other.y)
    }
}

impl<F: Float> Ord for Event<F> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.y.partial_cmp(&other.y).expect("fortune::Event Ord panic")
    }
}

struct BeachLine<F: Float> {
    nodes: Vec<BeachNode<F>>, // warning: can contain freed nodes
    free_nodes: Vec<usize>,
}

#[derive(Clone, Debug)]
enum BeachNode<F: Float> {
    Arc(Arc),
    Breakpoint(Breakpoint<F>),
}

#[derive(Clone, Debug)]
struct Arc {
    parent: Option<usize>,
    site: SiteId,
    circle_id: Option<CircleId>,
}

#[derive(Clone, Debug)]
struct Breakpoint<F: Float> {
    parent: Option<usize>,
    ray: Ray<F>,
    left: usize,
    right: usize,
}

#[derive(Clone, Debug)]
struct Ray<F: Float> {
    start: Point2<F>,
    direction: Vector2<F>,
    kind: RayKind<F>,
}

impl<F: Float> Ray<F> {
    fn direction(left: Point2<F>, right: Point2<F>) -> Vector2<F> {
        let d = left - right;
        Vector2::new(d.y, -d.x)
    }

    fn to_math(&self) -> math::Ray<F> {
        math::Ray(self.start, self.direction)
    }

    fn bind(&self, node: NodeId,
                   graph: &mut Graph<F>,
                   border_nodes: &mut BinaryHeap<BorderNode<F>>) {
        match self.kind {
            RayKind::GoingAway => panic!("trying to bind a ray going away to a voronoi node"),
            RayKind::FromOutside(side, point, a, b) => {
                let border_node = graph.add(Node::new(point));
                Data::bind_cell_node(border_node, a, graph);
                Data::bind_cell_node(border_node, b, graph);

                border_nodes.push(BorderNode {
                    side: side,
                    point: point,
                    node: border_node,
                });

                graph.link_edge(Edge {
                    u: border_node,
                    v: node,
                });
            }
            RayKind::FromInside(edge, part) => Data::bind_inside_ray(edge, part, node, graph),
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum RayKind<F: Float> {
    GoingAway,
    FromOutside(BorderSide, Point2<F>, SiteId, SiteId),
    FromInside(EdgeId, EdgePart),
}

#[derive(Clone, Copy, Debug)]
enum EdgePart {
    U,
    V,
}

impl<F: Float> BeachNode<F> {
    fn parent(&self) -> Option<usize> {
        match self {
            &BeachNode::Arc(ref a) => a.parent,
            &BeachNode::Breakpoint(ref b) => b.parent,
        }
    }

    fn set_parent(&mut self, parent: Option<usize>) {
        match self {
            &mut BeachNode::Arc(ref mut a) => a.parent = parent,
            &mut BeachNode::Breakpoint(ref mut b) => b.parent = parent,
        }
    }

    fn unwrap_arc(&self) -> &Arc {
        match self {
            &BeachNode::Arc(ref a) => a,
            _ => panic!("`BeachNode` is not a arc"),
        }
    }

    fn unwrap_arc_mut(&mut self) -> &mut Arc {
        match self {
            &mut BeachNode::Arc(ref mut a) => a,
            _ => panic!("`BeachNode` is not a arc"),
        }
    }

    fn unwrap_breakpoint(&self) -> &Breakpoint<F> {
        match self {
            &BeachNode::Breakpoint(ref b) => b,
            _ => panic!("`BeachNode` is not a breakpoint"),
        }
    }

    fn unwrap_breakpoint_mut(&mut self) -> &mut Breakpoint<F> {
        match self {
            &mut BeachNode::Breakpoint(ref mut b) => b,
            _ => panic!("`BeachNode` is not a breakpoint"),
        }
    }
}

impl<F: Float> BeachLine<F> {
    fn new() -> Self {
        BeachLine {
            nodes: Vec::new(),
            free_nodes: Vec::new(),
        }
    }

    fn clear(&mut self) {
        self.nodes.clear();
        self.free_nodes.clear();
    }

    fn root(&self) -> Option<usize> {
        if self.nodes.len() > self.free_nodes.len() {
            Some(0)
        } else {
            None
        }
    }

    fn add_node(&mut self, node: BeachNode<F>) -> usize {
        match self.free_nodes.pop() {
            Some(n) => {
                self.nodes[n] = node;
                n
            }
            None => {
                let n = self.nodes.len();
                self.nodes.push(node);
                n
            }
        }
    }

    fn free_node(&mut self, node: usize) {
        self.free_nodes.push(node);
    }

    fn get_arc_by_x(&self, x: F, sweep_y: F, sites: &[Site<F>]) -> usize {
        // there should be a root when the algorithm uses this function
        let mut node = self.root().unwrap();

        loop {
            let n = &self.nodes[node];
            match n {
                &BeachNode::Arc(_) => break,
                &BeachNode::Breakpoint(ref b) => {
                    node = if x < self.get_x_of_breakpoint(node, sweep_y, sites) {
                        b.left
                    } else {
                        b.right
                    };
                }
            }
        }

        node
    }

    fn get_x_of_breakpoint(&self, node: usize, sweep_y: F, sites: &[Site<F>]) -> F {
        // a breakpoint has to be formed by left and right arcs
        let la = self.get_closest_left_child_arc(node);
        let ra = self.get_closest_right_child_arc(node);

        let p = sites[self.get_arc(la).site.0];
        let r = sites[self.get_arc(ra).site.0];
        
        let two = F::from(2.).unwrap();
        let four = F::from(4.).unwrap();
        
        let compute_coefs = |s: Point2<F>| {
            let dp = two * (s.y - sweep_y);
            let a = F::one() / dp;
            let b = -two * s.x / dp;
            let c = sweep_y + dp / four + s.x * s.x / dp;
            (a, b, c)
        };

        let (a1, b1, c1) = compute_coefs(p);
        let (a2, b2, c2) = compute_coefs(r);

        let a = a1 - a2;
        let b = b1 - b2;
        let c = c1 - c2;

        let disc = b * b - four * a * c;
        let disc_sqrt = disc.sqrt();
        let x1 = (-b + disc_sqrt) / (two * a);
        let x2 = (-b - disc_sqrt) / (two * a);

        if p.y < r.y {
            x1.max(x2)
        } else {
            x1.min(x2)
        }
    }

    fn get_arc(&self, node: usize) -> &Arc {
        self.nodes[node].unwrap_arc()
    }

    fn get_arc_mut(&mut self, node: usize) -> &mut Arc {
        self.nodes[node].unwrap_arc_mut()
    }

    fn get_breakpoint(&self, node: usize) -> &Breakpoint<F> {
        self.nodes[node].unwrap_breakpoint()
    }

    fn get_breakpoint_mut(&mut self, node: usize) -> &mut Breakpoint<F> {
        self.nodes[node].unwrap_breakpoint_mut()
    }

    fn get_closest_left_parent(&self, node: usize) -> Option<usize> {
        let mut last = node;
        let mut maybe_parent = self.nodes[node].parent();

        loop {
            match maybe_parent {
                Some(parent) => {
                    let p = self.get_breakpoint(parent);
                    if p.left != last {
                        break;
                    }
                    last = parent;
                    maybe_parent = p.parent;
                }
                None => break,
            }
        }

        maybe_parent
    }

    fn get_closest_right_parent(&self, node: usize) -> Option<usize> {
        let mut last = node;
        let mut maybe_parent = self.nodes[node].parent();

        loop {
            match maybe_parent {
                Some(parent) => {
                    let p = self.get_breakpoint(parent);
                    if p.right != last {
                        break;
                    }
                    last = parent;
                    maybe_parent = p.parent;
                }
                None => break,
            }
        }

        maybe_parent
    }

    fn get_closest_left_child_arc(&self, breakpoint: usize) -> usize {
        let mut result = self.get_breakpoint(breakpoint).left;

        loop {
            let r = &self.nodes[result];
            match r {
                &BeachNode::Arc(_) => break,
                &BeachNode::Breakpoint(ref b) => result = b.right,
            }
        }

        result
    }

    fn get_closest_right_child_arc(&self, breakpoint: usize) -> usize {
        let mut result = self.get_breakpoint(breakpoint).right;

        loop {
            let r = &self.nodes[result];
            match r {
                &BeachNode::Arc(_) => break,
                &BeachNode::Breakpoint(ref b) => result = b.left,
            }
        }

        result
    }
}


struct ValidCircles {
    slots: BitVec,
    free_slots: Vec<usize>,
}

#[derive(Clone, Copy, Debug)]
struct CircleId(usize);

impl ValidCircles {
    fn new() -> Self {
        ValidCircles {
            slots: BitVec::new(),
            free_slots: Vec::new(),
        }
    }

    fn clear(&mut self) {
        self.slots.clear();
        self.free_slots.clear();
    }

    fn insert(&mut self) -> CircleId {
        let slot = match self.free_slots.pop() {
            Some(s) => {
                self.slots.set(s, true);
                s
            }
            None => {
                let s = self.slots.len();
                self.slots.push(true);
                s
            }
        };
        CircleId(slot)
    }

    fn contains(&self, id: CircleId) -> bool {
        self.slots.get(id.0).unwrap()
    }

    fn remove(&mut self, id: CircleId) {
        self.slots.set(id.0, false);
    }

    fn might_remove(&mut self, id: Option<CircleId>) {
        if let Some(id) = id {
            self.remove(id);
        }
    }

    fn release_id(&mut self, id: CircleId) {
        self.free_slots.push(id.0);
    }
}

struct Border<F: Float> {
    left: F,
    bottom: F,
    right: F,
    top: F,
}

impl<F: Float> Border<F> {
    fn from_rect(rect: &[F; 4]) -> Self {
        Border {
            left: rect[0],
            bottom: rect[1],
            right: rect[0] + rect[2],
            top: rect[1] + rect[3],
        }
    }

    fn inside(&self, p: &Point2<F>) -> bool {
        p.x >= self.left && p.y >= self.bottom && p.x <= self.right && p.y <= self.top
    }

    fn top_left(&self) -> Point2<F> {
        Point2::new(self.left, self.top)
    }
    fn bottom_left(&self) -> Point2<F> {
        Point2::new(self.left, self.bottom)
    }
    fn bottom_right(&self) -> Point2<F> {
        Point2::new(self.right, self.bottom)
    }
    fn top_right(&self) -> Point2<F> {
        Point2::new(self.right, self.top)
    }

    // TODO clean this up
    fn intersection(&self, ray: &math::Ray<F>) -> Option<(BorderSide, Point2<F>)> {
        let top_left = self.top_left();
        let bottom_left = self.bottom_left();
        let bottom_right = self.bottom_right();
        let top_right = self.top_right();

        let check_left = || {
            intersection(ray, &math::Segment(top_left, bottom_left)).map(|i| (BorderSide::Left, i))
        };
        let check_bottom = || {
            intersection(ray, &math::Segment(bottom_left, bottom_right))
                .map(|i| (BorderSide::Bottom, i))
        };
        let check_right = || {
            intersection(ray, &math::Segment(bottom_right, top_right))
                .map(|i| (BorderSide::Right, i))
        };
        let check_top = || {
            intersection(ray, &math::Segment(top_right, top_left)).map(|i| (BorderSide::Top, i))
        };

        let pos = |v, inf, sup| {
            if v < inf {
                -1i8
            } else if v > sup {
                1
            } else {
                0
            }
        };

        let start = ray.0;
        let x = pos(start.x, self.left, self.right);
        let y = pos(start.y, self.bottom, self.top);

        match (x, y) {
            (-1, -1) => check_left().or_else(|| check_bottom()),
            (-1, 0) => check_left(),
            (-1, 1) => check_left().or_else(|| check_top()),
            (0, -1) => check_bottom(),
            (0, 0) => {
                check_left()
                    .or_else(|| check_bottom())
                    .or_else(|| check_right())
                    .or_else(|| check_top())
            }
            (0, 1) => check_top(),
            (1, -1) => check_right().or_else(|| check_bottom()),
            (1, 0) => check_right(),
            (1, 1) => check_right().or_else(|| check_top()),
            _ => unreachable!(),
        }
    }
}
