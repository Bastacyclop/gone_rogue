pub mod graph;
pub use self::graph::{Graph, Cell, Node, Edge, CellId, NodeId, EdgeId};
pub mod fortune;

use cgmath::{Point2, EuclideanSpace};

use core::Float;

pub type Site<F: Float> = Point2<F>;

#[derive(Copy, Clone, Debug)]
pub struct SiteId(pub usize);

pub fn from_sites<F: Float>(sites: &mut [Site<F>], rect: &[F; 4], relaxation: usize) -> Graph<F> {
    let mut graph = Graph::new();
    let mut fortune = fortune::Data::new();

    lloyd(&mut fortune, &mut graph, sites, rect, relaxation);
    fortune.compute(sites, rect, &mut graph);

    graph
}

/// LLoyd's relaxation algorithm
pub fn lloyd<F: Float>(fortune: &mut fortune::Data<F>,
                       graph: &mut Graph<F>,
                       sites: &mut [Site<F>],
                       rect: &[F; 4],
                       iterations: usize) {
    for _ in 0..iterations {
        fortune.compute(sites, rect, graph);
        move_sites_to_centroid(sites, graph);
    }
}

fn move_sites_to_centroid<F: Float>(sites: &mut [Site<F>], graph: &Graph<F>) {
    for (site, s) in sites.iter_mut().enumerate() {
        let mut centroid = Point2::new(F::zero(), F::zero());
        let c = graph.get(CellId(site));
        for &node in &c.nodes {
            let n = graph.get(node);
            centroid = centroid + n.point.to_vec();
        }
        centroid = centroid / F::from(c.nodes.len()).unwrap();
        *s = centroid;
    }
}

#[cfg(test)]
mod tests {
    use test::Bencher;
    
    use core::unit::*;
    use random_sites;
    use super::*;

    #[bench]
    fn fortune_1600_sites(b: &mut Bencher) {
        let area = &[meters(-1.), meters(-1.), meters(2.), meters(2.)];
        let sites = random_sites(1000, area);
        let mut graph = Graph::new();
        let mut fortune = fortune::Data::new();

        b.iter(|| {
            fortune.compute(&sites, area, &mut graph);
        });
    }

    #[bench]
    fn fortune_10000_sites(b: &mut Bencher) {
        let area = &[meters(-1.), meters(-1.), meters(2.), meters(2.)];
        let sites = random_sites(10000, area);
        let mut graph = Graph::new();
        let mut fortune = fortune::Data::new();

        b.iter(|| {
            fortune.compute(&sites, area, &mut graph);
        });
    }
}
