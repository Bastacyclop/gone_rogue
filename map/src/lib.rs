#![feature(test)]

extern crate num;
extern crate cgmath;
extern crate bit_vec;
extern crate bit_set;
extern crate rand;
extern crate noise;
extern crate test;

extern crate core;
extern crate graphics;

pub mod voronoi;
mod lookup;

use cgmath::Point2;
use rand::Rng;

use core::unit::*;
use graphics::{Graphics, TRANSFORM_IDENTITY};
use lookup::Lookup;

pub type Graph = voronoi::Graph<Meters>;
pub type Site = voronoi::Site<Meters>;

pub struct Map {
    pub area: [Meters; 4],
    pub sites: Vec<Site>,
    pub graph: Graph,
    pub biomes: Vec<Biome>,
    pub borders: Vec<Border>,
    pub start: usize,
    pub current: usize,
    lookup: Lookup
}

#[derive(Debug, Clone)]
pub struct Biome {
    pub kind: BiomeKind,
    pub elevation: i16,
    pub temperature: u8,
    pub humidity: u8,
}

#[derive(Debug, Clone, Copy)]
pub enum BiomeKind {
    DeepWater,
    Water,
    Shore,
    Land,
    Mountain
}

pub struct Border {
    pub kind: BorderKind,
}

#[derive(Debug, Clone, Copy)]
pub enum BorderKind {
    Opened,
    Crossable,
    Closed
}

impl Map {
    pub fn with_hints(cell_count: usize, cell_size: Meters) -> Self {
        let size = cell_size * num::cast(cell_count).unwrap();
        let half = size / meters(2.0);
        let area = [-half, -half, size, size];
        
        Map::random(cell_count, area)
    }
    
    pub fn random(sites_sqrt: usize, area: [Meters; 4]) -> Self {
        let mut sites = random_sites(sites_sqrt * sites_sqrt, &area);
        let mut graph = voronoi::from_sites(&mut sites, &area, 2);
        let start = rand::thread_rng().gen_range(0, sites.len());
        graph.sort_cell_nodes_ccw(&sites);
        let biomes = random_biomes(&sites, &area, start);
        let borders = random_borders(&biomes, &graph);

        Map {
            lookup: Lookup::new(&graph, &sites, &area, sites_sqrt),
            area: area,
            sites: sites,
            start: start,
            current: start,
            graph: graph,
            biomes: biomes,
            borders: borders,
        }
    }
    
    pub fn lookup(&self, p: &Point2<Meters>) -> voronoi::CellId {
        self.lookup.lookup(p, &self.sites, &self.area)
    }
    
    pub fn render(&self, focused_cell: Option<voronoi::CellId>, graphics: &mut Graphics) {
        use graphics::polygon::translate_triangle;
        
        let color = graphics::pack_color((65, 65, 95, 255));
        
        let r = 0.5;
        let d = 2. * r;
        let mut point = Vec::new();
        graphics::polygon::ellipse([-r, -r, d, d], TRANSFORM_IDENTITY, 4, |&t| point.push(t));
        
        let to_piss = |p: Point2<Meters>| -> [f64; 2] {
            [p.x.get() as f64, p.y.get() as f64]
        };
        
        for (b, biome) in self.biomes.iter().enumerate() {
            let cell = &self.graph.cells[b];
            
            use BiomeKind::*;
            let color = graphics::pack_color(
                if Some(voronoi::CellId(b)) == focused_cell {
                    (255, 85, 85, 255)
                } else { match biome.kind {
                    DeepWater => (25, 55, 185, 255),
                    Water => (85, 135, 235, 255),
                    Shore => (225, 225, 135, 255),
                    Land => (85, 185, 125, 255),
                    Mountain => (165, 165, 135, 255)
                } }
            );
            
            let mut nodes = cell.nodes.iter().map(|&n| to_piss(self.graph.nodes[n.0].point));
            graphics.render(color, |render_triangle| {
                graphics::polygon::polygon(
                    TRANSFORM_IDENTITY,
                    || nodes.next(),
                    |t| render_triangle(t)
                );
            });  
        }
        
        for s in &self.sites {
            let w = s;
            let w = [w[0].get() as f32, w[1].get() as f32];
            
            graphics.render(color, |render_triangle| {
                for &t in &point {
                    render_triangle(&translate_triangle(t, w));
                }
            });
        }
    }
}

impl Biome {
    fn new(elevation: f32, temperature: f32, humidity: f32) -> Self {
        use self::BiomeKind::*;
        let kind = if elevation <= -0.3 {
            DeepWater
        } else if elevation <= 0.0 {
            Water
        } else if elevation <= 0.1 {
            Shore
        } else if elevation <= 0.7 {
            Land
        } else {
            Mountain
        };
        
        Biome {
            kind: kind,
            elevation: (elevation * (i16::max_value() as f32)) as i16,
            temperature: (((temperature + 1.0) / 2.0) * (u8::max_value() as f32)) as u8,
            humidity: (((humidity + 1.0) / 2.0) * (u8::max_value() as f32)) as u8,
        }
    }
}

pub fn random_sites(count: usize, area: &[Meters; 4]) -> Vec<Point2<Meters>> {
    let mut rng = rand::thread_rng();
    let mut sites = Vec::with_capacity(count);
    let inf_x = area[0].get();
    let inf_y = area[1].get();
    let sup_x = inf_x + area[2].get();
    let h = area[3].get();

    let y_step = h / count as f64;
    for i in 0..count {
        let x = rng.gen_range(inf_x, sup_x);
        let y = inf_y + i as f64 * y_step + rng.gen_range(0., y_step);
        sites.push(Point2::new(meters(x), meters(y)));
    }

    sites
}

pub fn random_biomes(sites: &[Site], area: &[Meters; 4], start: usize) -> Vec<Biome> {
    let mut rng = rand::thread_rng();
    let mut biomes = Vec::with_capacity(sites.len());

    let h_seed = rng.gen();
    let t_seed = rng.gen();
    let e_seed = rng.gen();
    
    let perlin = |seed, frequency, point: &Site| {
        noise::perlin2(seed, &[point.x.get() as f32 * frequency / area[2].get() as f32,
                               point.y.get() as f32 * frequency / area[3].get() as f32])
    };

    for site in sites {
        biomes.push(Biome::new(
            perlin(&e_seed, 1.6, &site),
            perlin(&t_seed, 0.75, &site),
            perlin(&h_seed, 2.0, &site)
        ));
    }

    biomes
}

pub fn random_borders(biomes: &[Biome], graph: &Graph) -> Vec<Border> {
    let mut rng = rand::thread_rng();
    let mut borders = Vec::with_capacity(graph.edges.len());
    
    for _ in &graph.edges {
        borders.push(Border {
            kind: rng.gen()
        });
    }
    
    borders
}

impl rand::Rand for BorderKind {
    fn rand<R: Rng>(rng: &mut R) -> Self {
        use self::BorderKind::*;
        const BORDER_KINDS: &'static [BorderKind] = &[Opened, Crossable, Closed];
        
        *rng.choose(BORDER_KINDS).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use rand::{self, Rng};
    use test::Bencher;
    use cgmath::Point2;
    
    use core::unit::*;
    use super::*;
    
    #[inline(always)]
    fn lookup_bench(b: &mut Bencher, sites_sqrt: usize) {
        let range = sites_sqrt as f64 / 2.;
        
        let mut rng = rand::thread_rng();
        let mut random = || Point2 {
            x: meters(rng.gen_range(-range, range)),
            y: meters(rng.gen_range(-range, range)),
        };
        
        let map = Map::with_hints(sites_sqrt, meters(1.0));
        
        b.iter(|| {
            map.lookup(&random());
        });
    }

    #[bench]
    fn lookup_1600_sites(b: &mut Bencher) {
        lookup_bench(b, 40);
    }

    #[bench]
    fn lookup_10000_sites(b: &mut Bencher) {
        lookup_bench(b, 100);
    }
}
