use num::{self, Float};
use cgmath::{Point2, InnerSpace};

use core::unit::*;
use voronoi::graph::CellId;
use {Graph, Site};

pub struct Lookup {
    sections: Vec<Section>,
    size: usize
}

#[derive(Debug)]
pub struct Section {
    cells: Vec<CellId>
}

impl Lookup {
    pub fn new(graph: &Graph, sites: &[Site], area: &[Meters; 4], size: usize) -> Lookup {
        let count = size * size;
        let mut sections = Vec::with_capacity(count);
        
        for _ in 0..count {
            sections.push(Section::new());
        }
        
        for (c, cell) in graph.cells.iter().enumerate() {
            let p = sites[c];
            let mut lower = p;
            let mut upper = p;
            
            for &n in &cell.nodes {
                let p = graph.get(n).point;
                lower.x = lower.x.min(p.x);
                lower.y = lower.y.min(p.y);
                upper.x = upper.x.max(p.x);
                upper.y = upper.y.max(p.y);
            }
            
            let (lower_sx, lower_sy) = Self::section_xy(&lower, area, size);
            let (upper_sx, upper_sy) = Self::section_xy(&upper, area, size);
            
            for sx in lower_sx..(upper_sx + 1) {
                for sy in lower_sy..(upper_sy + 1) {
                    sections[sx + sy * size].cells.push(CellId(c));
                }
            }
        }
        
        Lookup {
            sections: sections,
            size: size
        }
    }
    
    pub fn lookup(&self, p: &Point2<Meters>, sites: &[Site], area: &[Meters; 4]) -> CellId {
        let mut cell_ids = self.sections[Self::section(p, area, self.size)].cells.iter();
        
        let mut result = *cell_ids.next().expect("empty section");
        let mut d2 = (sites[result.0] - p).magnitude2();
        for &c in cell_ids {
            let c_d2 = (sites[c.0] - p).magnitude2();
            
            if c_d2 < d2 {
                result = c;
                d2 = c_d2;
            }
        }
        
        result
    }
    
    fn section(p: &Point2<Meters>, area: &[Meters; 4], size: usize) -> usize {
        let (sx, sy) = Self::section_xy(p, area, size);
        sx + sy * size
    }
    
    fn section_xy(p: &Point2<Meters>, area: &[Meters; 4], size: usize) -> (usize, usize) {
        let rx = p.x - area[0];
        let ry = p.y - area[1];
        let s = num::cast(size).unwrap();
        let section_w = area[2] / s;
        let section_h = area[3] / s;
        let sx = (rx / section_w).get() as usize;
        let sy = (ry / section_h).get() as usize;
        
        let clip = |x| if x > (size - 1) { (size - 1) } else { x };
        (clip(sx), clip(sy))
    }
}

impl Section {
    pub fn new() -> Section {
        Section {
            cells: Vec::new()
        }
    }
}
