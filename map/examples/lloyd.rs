extern crate piston;
extern crate cgmath;

extern crate core;
extern crate map;
extern crate graphics;

use piston::input::*;
use cgmath::Point2;

use core::unit::*;
use map::voronoi::{fortune, lloyd};
use graphics::{Camera, Graphics, TRANSFORM_IDENTITY};

const SITE_COUNT: usize = 40;

fn from_pixels(v: f64) -> f64 { v / 800.0 * 2.2 }

fn main() {
    let camera = Camera {
        size: meters(2.2),
        position: Point2::new(meters(0.), meters(0.))
    };
    
    let mut graphics = Graphics::new("Show Me Lloyd", 800, 800);
    graphics.set_ups(6);
    graphics.set_max_fps(60);

    let mut border = Vec::new();
    graphics::polygon::rectangle_border(
        [-1., -1., 2., 2.], from_pixels(0.5),
        TRANSFORM_IDENTITY, |&t| border.push(t)
    );
    let border_color = 0xAAAAAAFF;

    let area = &[meters(-1.), meters(-1.), meters(2.), meters(2.)];
    let mut sites = map::random_sites(SITE_COUNT, area);
    let mut graph = map::Graph::new();
    let mut fortune = fortune::Data::new();
    fortune.compute(&mut sites, area, &mut graph);

    let mut relax = false;

    while let Some(e) = graphics.next_event() {
        match e {
            Event::Render(_) => {
                graphics.clear([0.1, 0.1, 0.1, 1.0], &camera);
                graphics.render_slice(border_color, &border[..]);
                draw(&sites, &graph, &mut graphics);
                graphics.flush();
            }
            Event::Update(_) => {
                if relax {
                    lloyd(&mut fortune,
                          &mut graph,
                          &mut sites,
                          area,
                          1);
                    fortune.compute(&sites, area, &mut graph);
                }
            }
            Event::Input(Input::Release(_)) => {
                if relax {
                    sites = map::random_sites(SITE_COUNT, area);
                    fortune.compute(&sites, area, &mut graph);
                }
                relax = !relax;
            }
            _ => {}
        }
    }
}

fn to_win(p: &Point2<Meters>) -> [f64; 2] {
    [p.x.get() as f64, p.y.get() as f64]
}

pub fn draw(sites: &[map::Site],
            graph: &map::Graph,
            graphics: &mut Graphics) {
    let site_color = graphics::pack_color((125, 255, 25, 255));
    let voronoi_edge_color = graphics::pack_color((55, 55, 215, 255));
    let voronoi_node_color = graphics::pack_color((255, 215, 25, 255));
    let delaunay_edge_color = graphics::pack_color((215, 55, 55, 255));
    
    let r = from_pixels(2.0);
    let d = 2. * r;
    let l = from_pixels(0.5);

    for e in &graph.edges {
        let u_w = to_win(&graph.get(e.u).point);
        let v_w = to_win(&graph.get(e.v).point);

        graphics.render(voronoi_edge_color, |render_triangle| {
            graphics::polygon::line(u_w, v_w, l, TRANSFORM_IDENTITY, 2, |t| render_triangle(t));
        });
    }

    for n in &graph.nodes {
        let w = to_win(&n.point);
        
        graphics.render(voronoi_node_color, |render_triangle| {
            graphics::polygon::ellipse([w[0] - r, w[1] - r, d, d],
                                       TRANSFORM_IDENTITY, 8, |t| render_triangle(t));
        });
    }

    for (cell_a, ca) in graph.cells.iter().enumerate() {
        for cell_b in &ca.neighbors {
            let a_w = to_win(&sites[cell_a]);
            let b_w = to_win(&sites[cell_b.0]);
            
            graphics.render(delaunay_edge_color, |render_triangle| {
                graphics::polygon::line(a_w, b_w, l, TRANSFORM_IDENTITY, 2, |t| render_triangle(t));
            });
        }
    }
    
    for s in sites {
        let w = to_win(s);
        
        graphics.render(site_color, |render_triangle| {
            graphics::polygon::ellipse([w[0] - r, w[1] - r, d, d],
                                       TRANSFORM_IDENTITY, 8, |t| render_triangle(t));
        });
    }
}
