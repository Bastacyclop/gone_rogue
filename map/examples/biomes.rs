extern crate piston;
extern crate cgmath;

extern crate core;
extern crate map;
extern crate graphics;

use piston::input::*;
use cgmath::Point2;

use core::unit::*;
use map::Map;
use graphics::{Camera, Graphics, TRANSFORM_IDENTITY};


fn from_pixels(v: f64) -> f64 { v / 800.0 * 2.2 }

fn main() {
    let camera = Camera {
        size: meters(2.2),
        position: Point2::new(meters(0.), meters(0.))
    };
    
    let mut graphics = Graphics::new("Show Me Some Biomes", 800, 800);
    graphics.set_ups(1);
    graphics.set_max_fps(20);

    let mut border = Vec::new();
    graphics::polygon::rectangle_border(
        [-1., -1., 2., 2.], from_pixels(0.5),
        TRANSFORM_IDENTITY, |&t| border.push(t)
    );
    let border_color = 0xAAAAAAFF;
    
    let area = [meters(-1.), meters(-1.), meters(2.), meters(2.)];
    let map = Map::random(40, area);
    
    let mut drawed = false;
    let mut view = BIOME;

    while let Some(e) = graphics.next_event() {
        match e {
            Event::Render(_) => {
                if !drawed {
                    graphics.clear([0.1, 0.1, 0.1, 1.0], &camera);
                    graphics.render_slice(border_color, &border[..]);
                    draw(&map, view, &mut graphics);
                    graphics.flush();
                    
                    drawed = true;
                }
            }
            Event::Input(Input::Release(_)) => {
                view = (view + 1) % VIEW_COUNT;
                drawed = false;
            }
            _ => {}
        }
    }
}

const BIOME: u8 = 0;
const ELEVATION: u8 = 1;
const TEMPERATURE: u8 = 2;
const HUMIDITY: u8 = 3;
const VIEW_COUNT: u8 = 4;

fn to_win(p: &Point2<Meters>) -> [f64; 2] {
    [p.x.get() as f64, p.y.get() as f64]
}

pub fn draw(map: &map::Map, view: u8, graphics: &mut Graphics) {
    use graphics::polygon::translate_triangle;
    
    let color = graphics::pack_color((65, 65, 95, 255));
    
    let r = from_pixels(2.0);
    let d = 2. * r;
    let l = from_pixels(0.5);
    let mut point = Vec::new();
    graphics::polygon::ellipse([-r, -r, d, d], TRANSFORM_IDENTITY, 4, |&t| point.push(t));
    
    for (b, biome) in map.biomes.iter().enumerate() {
        let cell = &map.graph.cells[b];
        
        let color = graphics::pack_color(match view {
            BIOME => {
                use map::BiomeKind::*;
                match biome.kind {
                    DeepWater => (25, 55, 185, 255),
                    Water => (85, 135, 235, 255),
                    Shore => (225, 225, 135, 255),
                    Land => (85, 185, 125, 255),
                    Mountain => (165, 165, 135, 255)
                }
            }
            ELEVATION => {
                let f = u8::max_value() as f32 / i16::max_value() as f32;
                if biome.elevation <= 0 {
                    let e = (i16::max_value() + biome.elevation) as f32 * f;
                    (0, 0, e as u8, 255)
                } else {
                    let e = biome.elevation as f32 * f;
                    (e as u8, 255, e as u8, 255)
                }
            },
            TEMPERATURE => (biome.temperature, 0, 255 - biome.temperature, 255),
            HUMIDITY => (0, 0, biome.humidity, 255),
            _ => panic!("wrong view")
        });
        
        let mut nodes = cell.nodes.iter().map(|&n| to_win(&map.graph.nodes[n.0].point));
        graphics.render(color, |render_triangle| {
            graphics::polygon::polygon(
                TRANSFORM_IDENTITY,
                || nodes.next(),
                |t| render_triangle(t)
            );
        });  
    }
    
    for (b, border) in map.borders.iter().enumerate() {
        let edge = &map.graph.edges[b];
        let u_w = to_win(&map.graph.get(edge.u).point);
        let v_w = to_win(&map.graph.get(edge.v).point);
        
        use map::BorderKind::*;
        match border.kind {
            Closed => {
                graphics.render(color, |render_triangle| {
                    graphics::polygon::line(u_w, v_w, l, TRANSFORM_IDENTITY, 2, |t| render_triangle(t));
                });
            }
            Crossable => {
                graphics.render(color, |render_triangle| {
                    let d = [v_w[0] - u_w[0], v_w[1] - u_w[1]];
                    let a_w = [u_w[0] + 1. / 3. * d[0], u_w[1] + 1. / 3. * d[1]];
                    let b_w = [u_w[0] + 2. / 3. * d[0], u_w[1] + 2. / 3. * d[1]];
                    graphics::polygon::line(u_w, a_w, l, TRANSFORM_IDENTITY, 2, |t| render_triangle(t));
                    graphics::polygon::line(b_w, v_w, l, TRANSFORM_IDENTITY, 2, |t| render_triangle(t));
                });                
            }
            Opened => {}
        }
    }

    for n in &map.graph.nodes {
        let w = &n.point;
        let w = [w[0].get() as f32, w[1].get() as f32];
        
        graphics.render(color, |render_triangle| {
            for &t in &point {
                render_triangle(&translate_triangle(t, w));
            }
        });
    }
    
    for s in &map.sites {
        let w = s;
        let w = [w[0].get() as f32, w[1].get() as f32];
        
        graphics.render(color, |render_triangle| {
            for &t in &point {
                render_triangle(&translate_triangle(t, w));
            }
        });
    }
}
