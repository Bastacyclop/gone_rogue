use glue::StateProxy;
use glue::component::Storage;
use cgmath::{Rotation, Rotation2, Basis2, rad};

use graphics::Graphics;
use components::{Render, Transform};
use game::Context;

pub struct GraphicsProcess(());

impl GraphicsProcess {
    pub fn new() -> Self {
        GraphicsProcess(())
    }
    
    pub fn update(&mut self, graphics: &mut Graphics,
                             state: &StateProxy,
                             _context: &Context,
                             _delta: f64) {
        let renders = state.read::<Render>();
        let transforms = state.read::<Transform>();  
        for (entity, &Render { color, ref mesh }) in renders {            
            if let Some(transform) = transforms.get(entity) {
                let rotation = Basis2::from_angle(rad(transform.rotate));
                let transform = |point: [f32; 2]| -> [f32; 2] {
                    (rotation.rotate_point(point.into()) + transform.translate).into()
                };
                
                graphics.render(color, |render_triangle| {
                    for t in mesh {
                        render_triangle(&[transform(t[0]), transform(t[1]), transform(t[2])]);
                    }
                });
            } else {
                graphics.render_slice(color, &mesh[..]);
            }
        }
    }
}
