use glue::{StateProxy, EntityRef};
use wrapped2d::b2;
use wrapped2d::user_data::UserDataTypes;

use components::{Body, Transform};
use game::{Context, TIME_STEP};

pub struct PhysicsProcess(());

impl PhysicsProcess {
    pub fn new() -> Self {
        PhysicsProcess(())
    }
    
    pub fn fixed_update(&mut self, world: &mut World,
                                   state: &StateProxy,
                                   _context: &Context) {
        const VELOCITY_ITERATIONS: i32 = 8;
        const POSITION_ITERATIONS: i32 = 3;
        
        world.step(TIME_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
        world.clear_forces();
        
        let bodies = state.read::<Body>();
        let mut transforms = state.write::<Transform>();
        for (entity, body) in bodies {
            let body = world.get_body(body.handle);
            
            transforms.set(entity, Transform {
                translate: body.transform().pos.into(),
                rotate: body.transform().rot.angle()
            });
        }
    }
}

pub type World = b2::World<UserData>;

pub struct UserData;

impl UserDataTypes for UserData {
    type BodyData = EntityRef;
    type JointData = ();
    type FixtureData = ();
}
