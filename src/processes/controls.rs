use std::f32;
use glue::{StateProxy, EntityRef};
use wrapped2d::b2;
use cgmath::Point2;

use core::unit::*;
use components::*;
use processes::physics::World;
use events::*;
use game::{Context, TIME_STEP};

pub struct ControlsProcess {
    character: EntityRef,
    target: Option<Point2<Meters>>,
}

impl ControlsProcess {
    pub fn new(character: EntityRef) -> Self {
        ControlsProcess {
            character: character,
            target: None
        }
    }
    
    pub fn fixed_update(&mut self, state: &StateProxy, context: &mut Context, world: &World) {
        let character = state.accessor(self.character).unwrap();
        let handle = state.read::<Body>()[character].handle;
        let mut body = world.get_body_mut(handle);
        let position = *body.position();
        context.camera.position = Point2::new(
            meters(position.x as f64),
            meters(position.y as f64)
        );
        
        if let Some(t) = self.target {
            let dx = t.x.get() as f32 - position.x;
            let dy = t.y.get() as f32 - position.y;
            let distance = (dx*dx + dy*dy).sqrt();
            
            let precision = 0.001;
            if distance > precision {
                let max_speed = 10.;
                let speed = if distance > (max_speed * TIME_STEP) {
                    max_speed
                } else {
                    precision
                };
                
                let r = speed / distance;
                let v = b2::Vec2 {
                    x: dx * r,
                    y: dy * r
                };
                body.set_linear_velocity(&v);
                
                let next_angle = body.angle() +
                                 body.angular_velocity() * TIME_STEP;
                let rotation = v.y.atan2(v.x) - next_angle;
                let rotation = rotation % f32::consts::PI;
                let desired_angular_velocity = rotation / TIME_STEP;
                let torque = body.inertia() * desired_angular_velocity / TIME_STEP;
                body.apply_torque(torque, true);
            } else {
                body.set_linear_velocity(&b2::Vec2 { x: 0., y: 0. });
                self.target = None;
            }
        }
    }
    
    pub fn on_event(&mut self, event: GameEvent, context: &Context) {
        match event {
            GameEvent::Target => {
                let world = context.camera.window_to_world(&context.events.mouse);
                self.target = Some(world);
            }
        }
    }
}
