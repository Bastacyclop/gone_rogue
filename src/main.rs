extern crate glue;
extern crate wrapped2d;
extern crate num;
extern crate cgmath;
extern crate piston;
#[macro_use]
extern crate conrod;

extern crate core;
extern crate map;
extern crate graphics;
extern crate audio;

macro_rules! register_mod {
    ($register_method:ident:
     categories: [$($category:ident),*]
     items: [$(($snake:ident, $caml:ident)),*]) => {
        $(pub mod $category;)*
        $(pub mod $snake;)*
        
        pub mod items {
             $(pub use super::$category::items::*;)*
             $(pub use super::$snake::*;)*
        }
        
        use glue::State;
        #[allow(unused_variables)]
        pub fn register(state: &mut State) {
            $(self::$category::register(state);)*
            $(state.$register_method::<self::items::$caml>();)*
        }
    }
}

macro_rules! components_mod {
    (categories: [$($category:ident),*]
     items: [$(($snake:ident, $caml:ident)),*]) => {
        register_mod! {
            register_component:
            categories: [$($category),*]
            items: [$(($snake, $caml)),*]
        }
    }
}

macro_rules! prototypes_mod {
    (categories: [$($category:ident),*]
     items: [$(($snake:ident, $caml:ident)),*]) => {
        register_mod! {
            register_prototype:
            categories: [$($category),*]
            items: [$(($snake, $caml)),*]
        }
    }
}

macro_rules! groups_mod {
    (categories: [$($category:ident),*]
     items: [$(($snake:ident, $caml:ident)),*]) => {
        register_mod! {
            register_group:
            categories: [$($category),*]
            items: [$(($snake, $caml)),*]
        }
    }
}

mod components;
mod groups;
mod prototypes;
mod processes;
mod events;
mod game;
use game::Game;
mod spawn_helpers;

fn main() {
    Game::new().run()
}
