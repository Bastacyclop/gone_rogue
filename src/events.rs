use std::collections::HashMap;
use cgmath::Point2;
use piston::input::*;

#[derive(Clone, Copy, Debug)]
pub enum GameEvent {
    Target
}

pub struct Events {
    pub bindings: Bindings,
    pub mouse: Point2<f64>,
    pub mouse_scroll: Option<f64>,
}

impl Events {
    pub fn new() -> Self {
        Events {
            bindings: Bindings::default(),
            mouse: Point2::new(0., 0.),
            mouse_scroll: None
        }
    }
    
    pub fn clear(&mut self) {
        self.mouse_scroll = None;
    }
    
    pub fn pressed(&mut self, button: Button) -> Option<GameEvent> {
        self.bindings.press(&button)
    }
    
    pub fn released(&mut self, button: Button) -> Option<GameEvent> {
        self.bindings.release(&button)
    }
    
    pub fn moved(&mut self, m: Motion, half_w: f64, half_h: f64) {
        match m {
            Motion::MouseCursor(x, y) =>
                self.mouse = Point2::new((x - half_w) / half_w, (half_h - y) / half_h),
            Motion::MouseScroll(_, y) =>
                self.mouse_scroll = Some(y),
            _ => {}
        }
    }
}

pub struct Bindings {
    pub press: BindMap,
    pub release: BindMap,
}

pub type BindMap = HashMap<Button, GameEvent>;

impl Bindings {
    pub fn new() -> Self {
        Bindings {
            press: HashMap::new(),
            release: HashMap::new()
        }
    }
    
    pub fn bind_press(&mut self, button: Button, event: GameEvent) {
        self.press.insert(button, event);
    }
    
    pub fn bind_release(&mut self, button: Button, event: GameEvent) {
        self.release.insert(button, event);
    }
    
    pub fn press(&self, button: &Button) -> Option<GameEvent> {
        self.press.get(button).map(|&e| e)
    }
    
    pub fn release(&self, button: &Button) -> Option<GameEvent> {
        self.release.get(button).map(|&e| e)
    }
    
    pub fn clear(&mut self) {
        self.press.clear();
        self.release.clear();
    }
}

impl Default for Bindings {
    fn default() -> Self {
        let mut b = Bindings::new();
        b.bind_press(Button::Mouse(MouseButton::Left), GameEvent::Target);
        b
    }
}
