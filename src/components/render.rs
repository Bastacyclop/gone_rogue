use glue::*;

use graphics::polygon::Triangle;

#[derive(Clone, Debug)]
pub struct Render {
    pub color: u32,
    pub mesh: Vec<Triangle>
}

impl Component for Render {
    type Storage = component::storages::Packed<Self>;
}
