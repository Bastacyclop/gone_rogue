use glue::*;
use wrapped2d::b2;

#[derive(Clone, Debug)]
pub struct Body {
    pub handle: b2::BodyHandle
}

impl Component for Body {
    type Storage = component::storages::Packed<Self>;
}
