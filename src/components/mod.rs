components_mod! {
    categories: []
    items: [(body, Body), (transform, Transform), (render, Render)]
}

pub use self::items::*;
