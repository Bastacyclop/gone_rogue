use glue::*;
use cgmath::Vector2;

#[derive(Clone, Debug)]
pub struct Transform {
    pub translate: Vector2<f32>,
    pub rotate: f32,
}

impl Component for Transform {
    type Storage = component::storages::Packed<Self>;
}
