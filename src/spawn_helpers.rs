use glue::SpawnRequest;
use wrapped2d::b2;
use cgmath::Point2;

use core::unit::*;
use graphics::{self, TRANSFORM_IDENTITY};
use components::*;
use processes::physics::World;

pub trait SpawnHelpers {
    fn shape(self, shape: &[Point2<Meters>],
                       body_def: &b2::BodyDef,
                       color: u32,
                       world: &mut World) -> Self;
}

impl SpawnHelpers for SpawnRequest {
    fn shape(self, shape: &[Point2<Meters>],
                   body_def: &b2::BodyDef,
                   color: u32,
                   world: &mut World) -> Self {        
        let handle = world.create_body_with(body_def, self.entity_ref());
        
        let vertices: Vec<_> = shape.iter()
            .map(|p| b2::Vec2 { x: p.x.get() as f32, y: p.y.get() as f32 }).collect();
        let mut body_shape = b2::PolygonShape::new();
        body_shape.set(&vertices);
        
        world.get_body_mut(handle)
             .create_fast_fixture(&body_shape, 2.);
             
        let mut vertices = shape.iter().map(|p| [p.x.get(), p.y.get()]);
        let mut mesh = Vec::new();
        graphics::polygon::polygon(
            TRANSFORM_IDENTITY,
            || vertices.next(),
            |&t| mesh.push(t)
        );
             
        self.set(Body { handle: handle })
            .set(Render { color: color, mesh: mesh })
    }
}
