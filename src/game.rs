use std::f64;
use glue::{State, StateProxy, EntityRef, Scheduler};
use wrapped2d::b2;
use num::Float;
use cgmath::{Point2, Basis2, Rotation, Rotation2, InnerSpace, rad};
use piston::input::*;

use core::unit::*;
use map::Map;
use graphics::{self, Camera, Graphics};
use audio::Audio;
use components;
use groups;
use prototypes;
use processes::graphics::*;
use processes::physics::*;
use processes::controls::*;
use events::*;
use spawn_helpers::*;

pub struct Game {
    pub state: State,
    pub scheduler: Scheduler<Context>,
    pub context: Context,
    pub graphics: Graphics,
    pub world: World,
    pub map: Map,
}

pub struct Context {
    pub events: Events,
    pub camera: Camera,
    pub audio: Audio,
}

pub const UPDATES_PER_SECOND: u64 = 30;
pub const TIME_STEP: f32 = 1.0 / UPDATES_PER_SECOND as f32;

impl Game {
    pub fn new() -> Game {
        let mut state = State::new();
        components::register(&mut state);
        groups::register(&mut state);
        prototypes::register(&mut state);
        
        let mut graphics = Graphics::new("Gone Rogue", 800, 800);
        graphics.set_max_fps(60);
        graphics.set_ups(UPDATES_PER_SECOND);
        
        Game {
            state: state,
            scheduler: Scheduler::new(),
            context: Context::new(),
            graphics: graphics,
            world: World::new(&b2::Vec2 { x: 0., y: 0. }),
            map: Map::with_hints(20, meters(20.0)),
        }
    }
    
    pub fn run(mut self) {
        let character = self.initial_commit();
        
        let mut gp = GraphicsProcess::new();
        let mut pp = PhysicsProcess::new();
        let mut cp = ControlsProcess::new(character);
        
        let mut focused_cell = None;
        
        while let Some(e) = self.graphics.next_event() {
            match e {
                Event::Render(RenderArgs { ext_dt, .. }) => {
                    self.graphics.clear([0.1, 0.1, 0.2, 1.0], &self.context.camera);
                    
                    self.scheduler.kick_update(&mut self.state, &self.context, ext_dt);
                    self.map.render(focused_cell, &mut self.graphics);
                    
                    let mut state = self.state.update();
                    gp.update(&mut self.graphics, &state, &self.context, ext_dt);
                    state.commit();
                    
                    self.graphics.flush();
                }
                Event::Update(_) => {
                    self.scheduler.kick_fixed_update(&mut self.state, &self.context);
                    
                    let mut state = self.state.update();
                    pp.fixed_update(&mut self.world, &state, &self.context);
                    cp.fixed_update(&state, &mut self.context, &self.world);
                    state.commit();
                    
                    self.context.events.clear();
                }
                Event::Input(input) => {
                    match input {
                        Input::Press(button) => {
                            if let Some(e) = self.context.events.pressed(button) {
                                cp.on_event(e, &self.context);
                            }
                        }
                        Input::Release(button) => {
                            if let Some(e) = self.context.events.released(button) {
                                cp.on_event(e, &self.context);
                            }
                        }
                        Input::Move(motion) => {
                            self.context.events.moved(motion, 400., 400.);
                            
                            match motion {
                                Motion::MouseCursor(x, y) => {
                                    let window = Point2::new((x-400.) / 400., (400.-y) / 400.);
                                    let world = self.context.camera.window_to_world(&window);
                                    let cell_id = self.map.lookup(&world);
                                    println!("window({:.4}, {:.4}) -> world({:.1}, {:.1}) -> {:?}",
                                             window.x, window.y, world.x, world.y, cell_id);
                                    focused_cell = Some(cell_id);
                                }
                                Motion::MouseScroll(_, y) => {                            
                                    let s = self.context.camera.size - meters(y);
                                    self.context.camera.size = s.max(meters(1.))
                                                                .min(meters(1000.));
                                    println!("size: {}", s);
                                }
                                _ => {}
                            }
                        }
                        Input::Resize(..) => self.graphics.resize(),
                        _ => {}
                    }
                }
                _ => {}
            }
        }
    }
    
    pub fn initial_commit(&mut self) -> EntityRef {
        let mut state = self.state.update();
        Self::spawn_map_borders(&mut state, &mut self.map, &mut self.world);
        let character = Self::spawn_character(&mut state, &self.map, &mut self.world);
        state.commit();
        
        character
    }
    
    pub fn spawn_character(state: &mut StateProxy, map: &Map, world: &mut World) -> EntityRef {
        let color = graphics::pack_color((65, 25, 65, 255));
        let half_width = meters(0.5);
        
        let start = map.sites[map.start];
        
        let body_def = b2::BodyDef {
            body_type: b2::BodyType::Dynamic,
            position: b2::Vec2 { x: start.x.get() as f32, y: start.y.get() as f32 },
            .. b2::BodyDef::new()
        };
        
        let shape = [
            Point2::new(half_width, -half_width),
            Point2::new(half_width, half_width),
            Point2::new(-half_width, half_width),
            Point2::new(-half_width, -half_width),
        ];
        
        let request = state.spawn_request();
        let e_r = request.entity_ref();
        state.spawn_later(request.shape(&shape, &body_def, color, world));
        
        e_r
    }
    
    pub fn spawn_map_borders(state: &mut StateProxy, map: &mut Map, world: &mut World) {
        let color = graphics::pack_color((65, 65, 95, 255));
        let half_width = meters(0.5);
        
        let body_def = b2::BodyDef {
            body_type: b2::BodyType::Static,
            .. b2::BodyDef::new()
        };
        
        let mut spawn_border_body = |shape: &[_]| {
            state.spawn_later(state.spawn_request().shape(shape, &body_def, color, world));
        };
           
        for (b, border) in map.borders.iter().enumerate() {
            let edge = &map.graph.edges[b];
            let u = map.graph.get(edge.u).point;
            let v = map.graph.get(edge.v).point;
            let u_to_v = v - u;
            let u_to_v_norm = u_to_v.normalize_to(half_width);
            let border_beg = u + (-u_to_v_norm);
            let border_end = v + u_to_v_norm;
            
            let rotation = Basis2::from_angle(rad(meters(f64::consts::FRAC_PI_2)));
            let perp = rotation.rotate_vector(u_to_v_norm);
            
            use map::BorderKind::*;
            match border.kind {
                Closed => {
                    spawn_border_body(&[border_beg + perp, border_beg + (-perp),
                                        border_end + (-perp), border_end + perp]);
                }
                Crossable => {
                    let hole_beg = u + u_to_v * meters(1. / 3.);
                    let hole_end = u + u_to_v * meters(2. / 3.);
                    spawn_border_body(&[border_beg + perp, border_beg + (-perp),
                                        hole_beg + (-perp), hole_beg + perp]);
                    spawn_border_body(&[hole_end + perp, hole_end + (-perp),
                                        border_end + (-perp), border_end + perp]);
                }
                Opened => {}
            }
        }
        
    }
}

impl Context {
    pub fn new() -> Context {
        Context {
            camera: Camera::new(),
            events: Events::new(),
            audio: Audio::new(),
        }
    }
}
